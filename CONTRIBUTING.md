# Contributing to tus-calculator

Welcome to the tus-calculator project on GitLab! We're excited to have you contribute. This document outlines how you can get involved and make meaningful contributions.

## Code of Conduct

Before you start contributing, please read our [Code of Conduct](CODE_OF_CONDUCT.md). We expect all contributors to adhere to these guidelines to ensure a positive and inclusive community.

## How to Contribute

### Reporting Issues

If you encounter any issues with the project or have suggestions for improvements, please [create an issue](https://gitlab.socsci.ru.nl/fus-initiative/tus-calculator/-/issues/new) on our issue tracker. Be sure to provide as much detail as possible so we can understand and address the problem effectively.

### Making Contributions

1. Fork the repository on GitLab.
2. Clone your forked repository to your local machine.
3. Create a new branch for your feature or bug fix: `git checkout -b your-branch-name`.
4. Make your changes and ensure they follow our [style guidelines](link_to_style_guide).
5. Test your changes thoroughly.
6. Commit your changes with a descriptive commit message.
7. Push your changes to your forked repository: `git push origin your-branch-name`.
8. Submit a merge request (MR) to our repository. Describe your changes and the problem they solve clearly.

## Code Review

All MRs will be reviewed by our maintainers. Feedback or changes may be requested, so be prepared to make updates as necessary. Once your MR is approved, it will be merged into the main project.

## That's it!
Thank you for considering contributing to tus-calculator! Your contributions help make this project better for everyone.

Happy coding!
