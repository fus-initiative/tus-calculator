# TUS-calculator
The TUS-calculator application is under active development.

This calculator can be used to estimate derived quantities like ultrasound intensity and mechanical index, which have consequences for both safety and efficacy of ultrasound protocols.

The fundamental frequency, pressure and temporal regime of the intended protocol must be provided as inputs. Some derived parameters can also be provided as inputs. Please note that any manual changes in input parameters will automatically lead to adjustment of linked input parameters, in addition to updating of results. For instance, if MI is changed manually, the input fields for pressure amplitude, peak negative pressure and Isppa will be adjusted automatically.

Please see the ITRUSST standardized reporting and biophysical safety reports for further information about each derived quantity.

### Author

Original work by Dr. Kim Butts Pauly, Professor of Radiology, Stanford University

### Contributors
John van Hal, Radboud University, GitLab: @john.vanhal

ir. Margely Cornelissen, Radboud University, GitLab: @margely.cornelissen

Dr. Tulika Nandi, Vrije Universiteit Amsterdam https://research.vu.nl/en/persons/tulika-nandi

## License
This project is licensed under the [MIT License](LICENSE).

## Feedback
If you have any questions, suggestions, or feedback, please feel free to reach out to us via email at fus@ru.nl.
We'd love to hear from you and are always open to improving this project based on your input.

## Contribute
This project is open-source and open to contributions. Read more about [contributing to the TUS-calculator](CONTRIBUTING.md) and our [code of conduct](CODE_OF_CONDUCT.md).

## Disclaimer
This calculator is solely developed to estimate derived quantities like ultrasound intensity and mechanical index, which have consequences for both safety and efficacy of ultrasound protocols. It does not account for individual differences in anatomy, and is not intended to, and should not be used to, replace an individualized acoustic simulation. The calculator is not intended to be and should not be used as a medical device as defined in the Medical Device Regulation (Regulation (EU) 2017/745).

Use of the calculator is at one’s own risk: the calculator is provided “as is”, without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, safety, fitness for a particular purpose and non-infringement of any rights. In no event shall the creators, authors or copyright holders be liable for any claim, damages or other liability, arising from, out of or in connection with the calculator or the use of the calculator or data generated with or with the help of the calculator.

## Development environment
Upon successful build by the Gitlab pipeline, Tus-calculator will be deployed on the socsci gitlab-pages test/acceptance server.
The gitlab page is only accessible from the internal RU network.
> https://fus-initiative.gitlab-pages.socsci.ru.nl/tus-calculator/

## Production environment
Production TUS Calculator runs at:
> https://www.socsci.ru.nl/fusinitiative/tuscalculator/

## Stack
>- Vue3
>- State management: Pinia https://pinia.vuejs.org/
>- Component library: https://quasar.dev/
>- Unit-testing: Vitest & Vue-test-utils https://test-utils.vuejs.org/
>- Vue e-charts: https://github.com/ecomfe/vue-echarts

## Miscellaneous
Ultimo id: 0003417
