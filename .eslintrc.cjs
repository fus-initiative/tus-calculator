/* eslint-env node */
require('@rushstack/eslint-patch/modern-module-resolution')

module.exports = {
  root: true,
  parser: "vue-eslint-parser",
    parserOptions: {
        "parser": "@typescript-eslint/parser",
    },
    extends: [
        "plugin:vue/strongly-recommended",
        "eslint:recommended",
        // "@vue/typescript/recommended",
        "plugin:vitest/recommended"
    ],
    plugins: ["@typescript-eslint", "vitest"],
    rules: {
        // not needed for vue 3
        "vue/no-multiple-template-root": "off",
        "vue/no-v-model-argument": "off",
        "vue/ts-ignore": "off"
    }
}
