import { sentryVitePlugin } from "@sentry/vite-plugin";
import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue(), sentryVitePlugin({
    org: "tsg-k4",
    project: "tus-calculator"
  })],
  // eslint-disable-next-line no-undef
  base: process.env.NODE_ENV === "pages"
      ? 'https://fus-initiative.gitlab-pages.socsci.ru.nl/tus-calculator/'
      : 'https://www.socsci.ru.nl/fusinitiative/tuscalculator/',
  build: {
    minify: 'esbuild',
    sourcemap: true
  }
})