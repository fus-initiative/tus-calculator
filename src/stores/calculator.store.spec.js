import { setActivePinia, createPinia } from 'pinia'
import { vi, it, expect, describe } from 'vitest'
import { useCalculatorStore } from './calculator.store'
import '@vitest/web-worker'
import Worker from '../workers/pressureSignals?worker'

import {
    findMostConservativeValue, lookupTable,
    ultrasoundBeamDiameterArray,
    ultrasoundFrequencykHZArray
} from "./lookuptable"

vi.mock('quasar', async () => {
  // Import the actual module first to maintain other functionalities
  const actual = await vi.importActual('quasar');

  return {
    ...actual,
    Loading: {
      show: vi.fn(),
      hide: vi.fn(),
    },
  }
})

setActivePinia(createPinia())
const calculatorStore = useCalculatorStore()

describe('Calculator Store', () => {

  it('calculates on change pulse duration', () => {

    calculatorStore.$reset()
    calculatorStore.initWorker()
    calculatorStore.$patch({
        pulse_duration_ms: 7.5,
    })
    calculatorStore.updateByPr()
    expect(calculatorStore.ppp_mpa).toBe(0.79)
    expect(calculatorStore.pnp_mpa).toBe(0.79)
    expect(calculatorStore.isppa).toBe(19.26)
    expect(calculatorStore.MI).toBe(1.04)
  })


it('calculates on change MI', () => {
    calculatorStore.$reset()
    calculatorStore.initWorker()
    calculatorStore.$patch({
        isppaWorker: new Worker({type: 'module'}),
        ppp_mpa: 0.0,
        isppa: 0.0,
        ispta_pulse_train: 0.0,
        ispta_pulse_repetition: 0.0,
        MI: 1.04,
        derated_ppp_mpa: 0.0,
        derated_isppa: 0.0,
        derated_ispta_pulse_train: 0.0,
        derated_ispta_pulse_repetition: 0.0,
        derated_MI: 0.0,
        focal_depth_mm: 50,
        thickness_of_skull_mm: 10,
        thickness_of_scalp_mm: 5.8,
        coupling_thickness_mm: 10,
    })

    calculatorStore.updateByMI()

    expect(calculatorStore.ppp_mpa).toBe(0.79)
    expect(calculatorStore.pnp_mpa).toBe(0.79)
    expect(calculatorStore.MI).toBe(1.04)

    // formatting to significance is done in the view not in the store
    expect(calculatorStore.derated_ppp_mpa).toBe(0.41719300090006306)
    expect(calculatorStore.derated_pnp_mpa).toBe(0.41719300090006306)
    expect(calculatorStore.derated_MI).toBe( 0.59)
  })


})

it('calculates skull pressure transmission', async () => {
    // Test utility function outside of calculator store, because of getters depending on multiple input
    let frequency_kHz = 500
    let us_beam_diameter_on_skin_mm = 55

    let conservativeFreq = findMostConservativeValue(frequency_kHz, ultrasoundFrequencykHZArray)
    let conservativeDiameter = findMostConservativeValue(us_beam_diameter_on_skin_mm, ultrasoundBeamDiameterArray)
    let mostConservativeValue = lookupTable[`${conservativeFreq},${conservativeDiameter}`]
    let skullPressureTransmission = (mostConservativeValue / 100)

    expect(skullPressureTransmission).toBe(0.60)

    frequency_kHz = 510
    us_beam_diameter_on_skin_mm = 55

    conservativeFreq = findMostConservativeValue(frequency_kHz, ultrasoundFrequencykHZArray)
    conservativeDiameter = findMostConservativeValue(us_beam_diameter_on_skin_mm, ultrasoundBeamDiameterArray)
    mostConservativeValue = lookupTable[`${conservativeFreq},${conservativeDiameter}`]
    skullPressureTransmission = (mostConservativeValue / 100)

    expect(skullPressureTransmission).toBe(0.60)

    frequency_kHz = 499
    us_beam_diameter_on_skin_mm = 49

    conservativeFreq = findMostConservativeValue(frequency_kHz, ultrasoundFrequencykHZArray)
    conservativeDiameter = findMostConservativeValue(us_beam_diameter_on_skin_mm, ultrasoundBeamDiameterArray)
    mostConservativeValue = lookupTable[`${conservativeFreq},${conservativeDiameter}`]
    skullPressureTransmission = (mostConservativeValue / 100)

    expect(skullPressureTransmission).toBe(0.65)
})


it('handle findMostConservativeValue typeErrors', async () => {
    // Test utility function outside of calculator store, because of getters depending on multiple input
    let frequency_mHz = 1.7
    let frequency_kHz = frequency_mHz * 1000
    let us_beam_diameter_on_skin_mm = 0

    let conservativeFreq = findMostConservativeValue(frequency_kHz, ultrasoundFrequencykHZArray)
    let conservativeDiameter = findMostConservativeValue(us_beam_diameter_on_skin_mm, ultrasoundBeamDiameterArray)
    let mostConservativeValue = lookupTable[`${conservativeFreq},${conservativeDiameter}`]
    let skullPressureTransmission = (mostConservativeValue / 100)

    expect(skullPressureTransmission).toBe(0.5)

    frequency_kHz = 510
    us_beam_diameter_on_skin_mm = 55

    conservativeFreq = findMostConservativeValue(frequency_kHz, ultrasoundFrequencykHZArray)
    conservativeDiameter = findMostConservativeValue(us_beam_diameter_on_skin_mm, ultrasoundBeamDiameterArray)
    mostConservativeValue = lookupTable[`${conservativeFreq},${conservativeDiameter}`]
    skullPressureTransmission = (mostConservativeValue / 100)

    expect(skullPressureTransmission).toBe(0.6)

    frequency_kHz = 499
    us_beam_diameter_on_skin_mm = 49

    conservativeFreq = findMostConservativeValue(frequency_kHz, ultrasoundFrequencykHZArray)
    conservativeDiameter = findMostConservativeValue(us_beam_diameter_on_skin_mm, ultrasoundBeamDiameterArray)
    mostConservativeValue = lookupTable[`${conservativeFreq},${conservativeDiameter}`]
    skullPressureTransmission = (mostConservativeValue / 100)

    expect(skullPressureTransmission).toBe(0.65)
})
