import { defineStore } from 'pinia'
import JSZip from 'jszip'
import jspdf from "jspdf"
import autoTable from 'jspdf-autotable'
import Worker from '../workers/pressureSignals?worker'
import AcPowWorker from '../workers/averageAcousticPower.js?worker'
import {
  Loading,
  QSpinnerGears
} from 'quasar'

import {
    findMostConservativeValue,
    lookupTable,
    ultrasoundBeamDiameterArray,
    ultrasoundFrequencykHZArray
} from "./lookuptable.ts"

import FileSaver from 'file-saver'

export const useCalculatorStore = defineStore('calculator', {
    state: () => ({
        loading_processes: [],
        acoustic_power: 3750,
        average_acoustic_power: 1875,
        electrical_power: 5000,
        transducer_efficiency: 75,
        updateSeenTrainSection: false,
        canvasRefreshButton: true,
        showTimingGraph: false,
        confirmConditionsOfUse: false,
        isDarkModeGlobal: false,
        isTooltipEnabled: false,
        timingParameters: true,
        secondLevelTimingParameters: false,
        thirdLevelTimingParameters: false,
        plotImage: null,
        ppp_mpa: 0.79,
        pnp_mpa: 0.79,
        isppa: 19.26,
        ispta_pulse_train: 19.26,
        ispta_pulse_train_repetition: 19.26,
        MI: 1.04,
        derated_ppp_mpa: 0.44,
        derated_pnp_mpa: 0.44,
        derated_isppa: 6.04,
        derated_ispta_pulse_train: 6.04,
        derated_ispta_pulse_train_repetition: 6.04,
        derated_MI: 0.62,
        frequency_MHz: 0.5,
        derate: true,
        // assumptions
        rho_kg_m3: 1046,
        // rho_kg_m3_boneT: 1178,
        // rho_kg_m3_boneC: 1908,
        c_m_s: 1546.3,
        limits: {
          'MI': 1.9,
          'ISPPA': 190,
          'ISPTA': 0.72
        },
        // PULSE PARAMETERS
        pulse_duration_ms: 5,
        percentage_of_pri: 100,
        percentage_of_pd: 0,
        percentage_of_ptri: 100,
        percentage_pulse_train_repetition_ramp_width: 0,
        percentage_of_pulse_train_ramp_width: 0,
        pulse_ramp_duration_ms: 0,
        pulse_ramp_shape: "Tukey",
        pulse_repetition_interval_ms: 5,
        pulse_train_duration_ms: 5,
        pulse_train_ramp_duration_ms: 0,
        pulse_train_ramp_shape: "Tukey",
        pulse_train_repetition_interval_ms: 5,
        pulse_train_repetition_duration_ms: 5,
        pulse_train_repetition_ramp_duration_ms: 0,
        pulse_train_repetition_ramp_shape: "Tukey",
        Specific_Heat_J_kg_C: 3600,
        Specific_Heat_J_kg_C_BONE_T: 2274,
        Specific_Heat_J_kg_C_BONE_C: 1313,
        Attenuation_db_cm_MHz: 0.5,
        Attenuation_db_cm_MHz_BRAIN: 0.5,
        Attenuation_db_cm_MHz_SKULL_T: 4.69,
        Attenuation_db_cm_MHz_SKULL_C: 4.08,
        diameter_of_transducer_mm: 64,
        focal_depth_mm: 50,
        thickness_of_skull_mm: 10,
        thickness_of_scalp_mm: 5.8,
        coupling_thickness_mm: 10,
        isManualSkullPressureTransmission: false,
        skull_pressure_transmission_manual: 0,
        isEqualPPPPNP: true,
        pulse_train_repetition_duration_max: 18000000,
        isppaFromPressureSignal: [],
        isppaWorker: null,
        avgAcPowWorker: null,
        isppaLoading: {
            pulse: null,
            train: null,
            repetition: null
        },
        attenuationCoefficient: 0.87,
        absorptionFraction: 0.34,
        rampShapes: ['Tukey','Linear'],
        // This mapper is only used to map uploaded JSON or YAML keys back to the state properties
        mapper: {
            ppp_mpa: "Pressure Amplitude [MPa]",
            "Pressure Amplitude [MPa]": "ppp_mpa",
            pnp_mpa: "Peak Negative Pressure [MPa]",
            "Peak Negative Pressure [MPa]": "pnp_mpa",
            isppa: "Isppa [W/cm2]",
            "MI": "MI",
            "Isppa [W/cm2]": "isppa",
            frequency_MHz: "Fundamental Frequency [MHz]",
            "Fundamental Frequency [MHz]": "frequency_MHz",
            rho_kg_m3: "Density of brain [kg/m3]",
            "Density of brain [kg/m3]": "rho_kg_m3",
            c_m_s: "Speed Of Sound of Brain [m/s]",
            "Speed Of Sound of Brain [m/s]": "c_m_s",
            z_Rayl: "Acoustic Impedance [kg/(mm2*s)]",
            "Acoustic Impedance [kg/(mm2*s)]": "z_Rayl",
            diameter_of_transducer_mm: "Diameter of transducer [mm]",
            "Diameter of transducer [mm]": "diameter_of_transducer_mm",
            focal_depth_mm: "Focal depth [mm]",
            "Focal depth [mm]": "focal_depth_mm",
            "thickness_of_skull_mm": "Skull thickness [mm]",
            "Skull thickness [mm]": "thickness_of_skull_mm",
            "thickness_of_scalp_mm": "Scalp thickness [mm]",
            "Scalp thickness [mm]": "thickness_of_scalp_mm",
            "coupling_thickness_mm": "Coupling thickness [mm]",
            "Coupling thickness [mm]": "coupling_thickness_mm",
            "us_beam_diameter_on_skin_mm": "US beam diameter on skin [mm]",
            "US beam diameter on skin [mm]": "us_beam_diameter_on_skin_mm",
            "us_beam_diameter_on_skull_mm": "US beam diameter on skull [mm]",
            "US beam diameter on skull [mm]": "us_beam_diameter_on_skull_mm",
            "skull_pressure_transmission_manual": "Skull pressure transmission [-]",
            "Skull pressure transmission [-]": "skull_pressure_transmission_manual",
            "Pulse Duration [ms]": "pulse_duration_ms",
            "Pulse Ramp Shape": "pulse_ramp_shape",
            "Ramp Duration [ms]": "pulse_ramp_duration_ms",
            "Pulse Repetition Interval [ms]": "pulse_repetition_interval_ms",
            "Pulse Repetition Frequency [Hz]": "pulse_repetition_frequency_Hz",
            "Pulse Train Duration [ms]": "pulse_train_duration_ms",
            "Pulse Train Ramp Duration [ms]": "pulse_train_ramp_duration_ms",
            "Pulse Train Ramp Shape": "pulse_train_ramp_shape",
            "Pulse Train Repetition Interval [ms]": "pulse_train_repetition_interval_ms",
            "Pulse Train Repetition Frequency [Hz]": "pulse_train_repetition_frequency_Hz",
            "Pulse Train Repetition Duration [ms]": "pulse_train_repetition_duration_ms",
            "Pulse Train Repetition Ramp Duration [ms]": "pulse_train_repetition_ramp_duration_ms",
            "Pulse Train Repetition Ramp Shape": "pulse_train_repetition_ramp_shape",
            "Ispta of pulse train [W/cm2]": "ispta_pulse_train",
            "Ispta of pulse train repetition [W/cm2]": "ispta_pulse_train_repetition",
            "Brain attenuation coefficient [db/cm/MHz]": "attenuationCoefficient",
            "Brain absorption fraction [-]": "absorptionFraction",
            "Pulse Train is enabled": "secondLevelTimingParameters",
            "Pulse Train Repetition is enabled": "thirdLevelTimingParameters",
        },
        ppp_mpa_unformatted: null,
        derated_ppp_mpa_unformatted: null,
        parametersChanged: false
    }),
    getters: {
        getTIC: (state) => +(+state.average_acoustic_power / +state.us_beam_diameter_on_skull_cm / 40).toFixed(1),
        pulseOnly: (state) => state.timingParameters && !state.secondLevelTimingParameters,
        pulseAndTrainOnly: (state) => state.timingParameters && state.secondLevelTimingParameters && !state.thirdLevelTimingParameters,
        allTimeTogglesOn:(state) => state.timingParameters && state.secondLevelTimingParameters && state.thirdLevelTimingParameters,
        //att_coef_abs value = attenuation coefficient x absorption fraction. Attenuation coefficient = 0.87 and absorption fraction = 0.34
        att_coef_abs: (state) => +(state.attenuationCoefficient * state.absorptionFraction).toFixed(2),
        primary_parameters_valid: (state) => {
            return (
                state.ppp_mpa > 0
                && state.isppa > 0
                && state.MI > 0
                && state.frequency_MHz > 0
                && state.rho_kg_m3 > 0
                && state.c_m_s > 0
                && state.diameter_of_transducer_mm > 0
                && state.focal_depth_mm > 0
                && state.thickness_of_skull_mm > 0
                && state.thickness_of_scalp_mm > 0
                && state.coupling_thickness_mm > 0
                && state.us_beam_diameter_on_skin_mm > 0
                && state.skull_pressure_transmission > 0
                && state.brain_pressure_transmission > 0
                && state.coeff_dB_cm_MHz > 0
                && state.absorptionFraction > 0
                && state.attenuationCoefficient > 0
            )
        },
        time_parameters_gt_0: (state) => {
            return (state.pulse_duration_ms > 0
                && state.percentage_of_pri > 0
                && state.percentage_of_pd >= 0
                && state.percentage_of_ptri > 0
                && state.percentage_pulse_train_repetition_ramp_width >= 0
                && state.pulse_ramp_duration_ms >= 0
                && state.pulse_repetition_interval_ms > 0
                && state.pulse_train_duration_ms  > 0
                && state.pulse_train_ramp_duration_ms >= 0
                && state.pulse_train_repetition_interval_ms > 0
                && state.pulse_train_repetition_duration_ms > 0
                && state.pulse_train_repetition_ramp_duration_ms >= 0
          )
        },
        time_parameters_valid: (state) => {
            const ramp_duration_limit = +state.pulse_duration_ms / 2
            const pulse_train_ramp_duration_limit = (+state.pulse_train_duration_ms / 2) - (Math.floor(state.pulse_repetition_interval_ms - state.pulse_duration_ms) / 2)
            const pulse_train_repetition_ramp_duration_limit = (+state.pulse_train_repetition_duration_ms - Math.floor(state.pulse_repetition_interval_ms - state.pulse_duration_ms)) / 2
            return (!((+state.pulse_ramp_duration_ms > ramp_duration_limit)
                // The repetition interval should always be larger than the pulse duration.
                || (state.pulse_repetition_interval_ms < +state.pulse_duration_ms)
                // Don't allow a Pulse Train Repetition Interval smaller than Pulse Train Duration (equal is allowed).
                || (+state.pulse_train_repetition_interval_ms < +state.pulse_train_duration_ms)
                // Don't allow a Pulse Train Duration smaller than the Pulse Repetition Interval (equal is allowed).
                // || (+state.pulse_train_duration_ms < +state.pulse_repetition_interval_ms)
                // Don't allow a Pulse Train Repetition Duration smaller than the Pulse Train Repetition Interval (equal is allowed).
                || (+state.pulse_train_repetition_duration_ms < +state.pulse_train_repetition_interval_ms)
                || (+state.pulse_train_repetition_duration_ms > state.pulse_train_repetition_duration_max)
                // Check validity pulse_train_ramp_duration_ms
                || (+state.pulse_train_ramp_duration_ms > pulse_train_ramp_duration_limit
                // Check validity pulse_train_repetition_ramp_duration_ms
                || +state.pulse_train_repetition_ramp_duration_ms > pulse_train_repetition_ramp_duration_limit))
            )
        },
        us_beam_diameter_on_skin_mm: (state) => {
            // US beam diameter on skin [mm] -> calculated in the following way
            // Equations used in the background:
            // Radius = Diameter of transducer / 2
            // angle [degrees] = arctan(Radius / Focal depth)
            // radius decrease = tan(angle) * Distance between exit plan and skin
            // US beam diameter on skin = Diameter of transducer - (radius decrease * 2)`
            const radius = +state.diameter_of_transducer_mm / 2
            const angle = Math.atan(radius / +state.focal_depth_mm)
            const radius_decrease = Math.tan(angle) * +state.coupling_thickness_mm
            return +(state.diameter_of_transducer_mm - +(radius_decrease * 2)).toFixed(1)
        },
        us_beam_diameter_on_skull_mm: (state) => {
            // US beam diameter on skull [Mm] -> calculated in the following way
            // Equations used in the background:
            // Radius = Diameter of transducer / 2
            // angle [degrees] = arctan(Radius / Focal depth)
            // radius decrease = tan(angle) * Distance between exit plan and skin
            // US beam diameter on skull = Diameter of transducer - (radius decrease * 2)`
            const radius = state.diameter_of_transducer_mm / 2
            const angle = Math.atan(radius / state.focal_depth_mm)
            const radius_decrease = Math.tan(angle) * (+state.coupling_thickness_mm + +state.thickness_of_scalp_mm)
            return +(state.diameter_of_transducer_mm - (radius_decrease * 2)).toFixed(1)
        },
           us_beam_diameter_on_skull_cm: (state) => {
            return +(state.us_beam_diameter_on_skull_mm  / 10).toFixed(2)
        },
        distance_in_brain_cm: (state) => (+state.focal_depth_mm - +state.thickness_of_skull_mm - +state.thickness_of_scalp_mm - +state.coupling_thickness_mm) / 10,
        alpha_brain: (state) => ((0.5 * (state.derate == 1)) + (0 * (state.derate == 0))),
        getPressureTransmission: (state) => {
            const conservativeFreq = findMostConservativeValue(state.frequency_MHz * 1000, ultrasoundFrequencykHZArray);
            const conservativeDiameter = findMostConservativeValue(state.us_beam_diameter_on_skull_mm, ultrasoundBeamDiameterArray);
            const mostConservativeValue = lookupTable[`${conservativeFreq},${conservativeDiameter}`];
            return (mostConservativeValue / 100)
        },
        // d_reas_coeff: (state) => state.getPressureTransmission,
        d_reas_coeff: (state) => state.isManualSkullPressureTransmission ? state.skull_pressure_transmission_manual : state.getPressureTransmission,
        energy_deposition: (state) => state.calculate_energy_deposition('freefield'), // energy deposition
        derated_energy_deposition: (state) => state.calculate_energy_deposition('derated'),
        pulse_duration_in_s: (state) => state.pulse_duration_ms / 1000,
        pulse_train_duration_in_s: (state) => state.pulse_train_duration_ms / 1000,
        pulse_train_repetition_duration_in_s: (state) => state.pulse_train_repetition_duration_ms / 1000,
        rate_of_energy_deposition: (state) => {
            if (state.pulseOnly) {
                return +(state.energy_deposition / state.pulse_duration_ms).toFixed(3)
            } else if (state.pulseAndTrainOnly) {
                return +(state.energy_deposition / state.pulse_train_duration_ms).toFixed(3)
            } else {
                return +(state.energy_deposition / state.pulse_train_repetition_duration_in_s).toFixed(3)
            }
        },
        derated_rate_of_energy_deposition: (state) => {
            if (state.pulseOnly) {
                return +(state.derated_energy_deposition / state.pulse_duration_ms).toFixed(3)
            } else if (state.pulseAndTrainOnly) {
                return +(state.derated_energy_deposition / state.pulse_train_duration_ms).toFixed(3)
            } else {
                return +(state.derated_energy_deposition / state.pulse_train_repetition_duration_in_s).toFixed(3)
            }
        },
        brain_pressure_transmission: (state) => +(Math.exp((-state.distance_in_brain_cm) * state.frequency_MHz * state.alpha_brain / 8.686).toFixed(3)),
        z_kg_s_m2: (state) =>  state.round(state.rho_kg_m3 * state.c_m_s),
        z_Rayl: (state) =>  state.round(state.z_kg_s_m2 / 1000000),
        P_Ave_norm: (state) => (state.pulse_duration_ms - state.pulse_ramp_duration_ms) / state.pulse_duration_ms,
        DC_pulse_train: (state) => state.pulse_duration_ms / state.pulse_repetition_interval_ms,
        DC_pulse_train_repetition: (state) => state.pulse_train_duration_ms / state.pulse_train_repetition_interval_ms,
        skull_pressure_transmission: (state) => +(state.d_reas_coeff).toFixed(3),
        coeff_dB_cm_MHz: (state) => state.alpha_brain,
        coeff_np_cm_MHz: (state) => state.alpha_brain / 8.686,
        // temp rise
        integral_duration_s: (state) => +state.pulse_duration_ms / 1000 * +state.pulse_train_duration_ms / +state.pulse_repetition_interval_ms,
        Density_kg_cm3: (state) => state.rho_kg_m3 / 10 ** 6,
        two_alphaI_rhoCt: (state) => state.calculate_two_alphaI_rhoCt(
            (state.att_coef_abs * state.frequency_MHz) / 8.686,
            state.isppa,
            state.Density_kg_cm3,
            state.Specific_Heat_J_kg_C
        ),
        two_alphaI_rhoCt_derated: (state) => state.calculate_two_alphaI_rhoCt(
            (state.att_coef_abs * state.frequency_MHz) / 8.686,
            state.derated_isppa,
            state.Density_kg_cm3,
            state.Specific_Heat_J_kg_C
        ),
        Temp_Rise_C: state => (state.two_alphaI_rhoCt * state.integral_duration_s),
        Temp_Rise_C_derated: state => (state.two_alphaI_rhoCt_derated * state.integral_duration_s),
        tableConfigurations: (state) => {
            return {
                notderated: {
                    columns: [
                        {title: "Pressure Amplitude [MPa]", dataKey: "ppp_mpa"},
                        {title: "Peak Negative pressure [MPa]", dataKey: "pnp_mpa"},
                        {title: "Isppa [W/cm2]", dataKey: "isppa"},
                        {title: "Fundamental Frequency", dataKey: "frequency_MHz"},
                        {title: "Ispta of pulse train [W/cm2]", dataKey: "ispta_pulse_train"},
                        {title: "Ispta of pulse train repetition [W/cm2]", dataKey: "ispta_pulse_train_repetition"},
                        {title: "delta °C in brain", dataKey: "temp_rise"},
                        {title: "Energy deposition [J/cm2]", dataKey: "energy_deposition"},
                        {title: "Rate of energy deposition [J/(cm2 * s)]", dataKey: "rate_energy_deposition"},
                    ],
                    rows: [{
                        "ppp_mpa": state.ppp_mpa,
                        "pnp_mpa": state.pnp_mpa,
                        "isppa": state.isppa,
                        "frequency_MHz": state.frequency_MHz,
                        "ispta of pulse train": state.ispta_pulse_train,
                        "ispta of pulse train repetition": state.ispta_pulse_train_repetition,
                        "temp_rise": state.Temp_Rise_C.toFixed(4),
                        "energy_deposition": state.energy_deposition,
                        "rate_of_energy_deposition": state.rate_of_energy_deposition
                    }]
                },
                derated : {
                    columns: [
                        {title: "Pressure Amplitude [MPa]", dataKey: "derated_ppp_mpa"},
                        {title: "Peak Negative pressure [MPa]", dataKey: "derated_pnp_mpa"},
                        {title: "Isppa [W/cm2]", dataKey: "derated_isppa"},
                        {title: "MI", dataKey: "MI"},
                        {title: "MI_tc", dataKey: "derated_MI"},
                        {title: "Fundamental Frequency", dataKey: "frequency_MHz"},
                        {title: "Ispta of pulse train [W/cm2]", dataKey: "derated_ispta_of_pulse_train"},
                        {title: "Ispta of pulse train repetition [W/cm2]", dataKey: "derated_ispta_of_pulse_train_repetition"},
                        {title: "delta °C in brain", dataKey: "Temp_Rise_C_derated"},
                        {title: "Energy deposition [J/cm2]", dataKey: "energy_deposition"},
                        {title: "Rate of energy deposition [J/(cm2 * s)]", dataKey: "rate_energy_deposition"},

                    ],
                    rows: [{
                        "ppp_mpa": state.derated_ppp_mpa,
                        "pnp_mpa": state.derated_pnp_mpa,
                        "isppa": state.derated_isppa,
                        "MI": state.MI,
                        "MI_tc": state.derated_MI,
                        "frequency_MHz": state.frequency_MHz,
                        "ispta of pulse train": state.derated_ispta_pulse_train,
                        "ispta of pulse train repetition": state.derated_ispta_pulse_train_repetition,
                        "temp_rise": state.Temp_Rise_C_derated.toFixed(4),
                        "energy_deposition": state.derated_energy_deposition,
                        "rate_of_energy_deposition": state.derated_rate_of_energy_deposition
                    }]
                },
                timing: {
                    columns: [
                        {title: "Pulse duration [ms]", dataKey: "pulse_duration_ms"},
                        {title: "Pulse Ramp duration [ms]", dataKey: "pulse_ramp_duration_ms"},
                        {title: "Pulse Ramp Shape", dataKey: "pulse_ramp_shape"},
                        {title: "Pulse Repetition interval [ms]", dataKey: "pulse_repetition_interval_ms"},
                        {title: "Pulse Train Duration [ms]", dataKey: "pulse_train_duration_ms"},
                        {title: "Pulse Train Ramp Duration [ms]", dataKey: "pulse_train_ramp_duration_ms"},
                        {title: "Pulse Train Ramp Shape", dataKey: "pulse_train_ramp_shape"},
                        {title: "Pulse Train Repetition Interval [ms]", dataKey: "pulse_train_repetition_interval_ms"},
                        {title: "Pulse Train Repetition Duration [ms]", dataKey: "pulse_train_repetition_duration_ms"},
                        {title: "Pulse Train Repetition Ramp Duration [ms]", dataKey: "pulse_train_repetition_ramp_duration_ms"},
                        {title: "Pulse Train Repetition Ramp Shape", dataKey: "pulse_train_repetition_ramp_shape"},

                    ],
                    rows: [{
                        "pulse_duration_ms": state.pulse_duration_ms,
                        "pulse_ramp_duration_ms": state.pulse_ramp_duration_ms,
                        "pulse_ramp_shape": state.pulse_ramp_shape,
                        "pulse_repetition_interval_ms": state.pulse_repetition_interval_ms,
                        "pulse_train_duration_ms": state.pulse_train_duration_ms,
                        "pulse_train_ramp_duration_ms": state.pulse_train_ramp_duration_ms,
                        "pulse_train_ramp_shape": state.pulse_train_ramp_shape,
                        "pulse_train_repetition_interval_ms": state.pulse_train_repetition_interval_ms,
                        "pulse_train_repetition_duration_ms": state.pulse_train_repetition_duration_ms,
                        "pulse_train_repetition_ramp_duration_ms": state.pulse_train_repetition_ramp_duration_ms,
                        "pulse_train_repetition_ramp_shape": state.pulse_train_repetition_ramp_shape
                    }]
                },
                assumptions: {
                    columns: [
                        {title: "Density of brain [kg/m3]", dataKey: "rho_kg_m3"},
                        {title: "Speed of Sound of Brain [m/s]", dataKey: "c_m_s"},
                        {title: "Acoustic Impedance [kg/(mm2*s)]", dataKey: "z_Rayl"},
                    ],
                    rows: [{
                        "rho_kg_m3": state.rho_kg_m3,
                        "c_m_s": state.c_m_s,
                        "z_Rayl": state.z_Rayl,
                    }]
                },
                derating_values: {
                    columns: [
                        {title: "Diameter of transducer [mm]", dataKey: "diameter_of_transducer_mm"},
                        {title: "Focal depth [mm]", dataKey: "focal_depth_mm"},
                        {title: "Skull thickness [mm]", dataKey: "thickness_of_skull_mm"},
                        {title: "Scalp thickness [mm]", dataKey: "thickness_of_scalp_mm"},
                        {title: "Coupling thickness [mm]", dataKey: "coupling_thickness_mm"},
                        {title: "US beam diameter on skin [mm]", dataKey: "us_beam_diameter_on_skin_mm"},
                        {title: "US beam diameter on skull [mm]", dataKey: "us_beam_diameter_on_skull_mm"},
                        {title: "Skull pressure transmission [-]", dataKey: "skull_pressure_transmission"},
                        {title: "Brain pressure transmission [-]", dataKey: "brain_pressure_transmission"},
                        {title: "Attenuation coefficient [dB/cm/MHz]", dataKey: "attenuation_coefficient"},
                    ],
                    rows: [{
                        "diameter_of_transducer_mm": state.diameter_of_transducer_mm,
                        "focal_depth_mm": state.focal_depth_mm,
                        "thickness_of_skull_mm": state.thickness_of_skull_mm,
                        "thickness_of_scalp_mm": state.thickness_of_scalp_mm,
                        "coupling_thickness_mm": state.coupling_thickness_mm,
                        "us_beam_diameter_on_skin_mm": state.us_beam_diameter_on_skin_mm,
                        "us_beam_diameter_on_skull_mm": state.us_beam_diameter_on_skull_mm,
                        "skull_pressure_transmission": state.isManualSkullPressureTransmission ? state.skull_pressure_transmission_manual : state.skull_pressure_transmission,
                        "brain_pressure_transmission": state.brain_pressure_transmission,
                        "attenuation_coefficient": state.coeff_dB_cm_MHz,
                    }]
                }
            }
        },
        depth_cm: (state) => (state.focal_depth_mm - +state.coupling_thickness_mm) / 10,
        depth_mm: (state) => state.focal_depth_mm - +state.coupling_thickness_mm
    },
    actions: {
        activateCanvasRefreshButton() {
          this.canvasRefreshButton = true
        },
        mapToStateKeys(obj) {
            const newObj = {}
            for (const key in obj) {
                if (Object.prototype.hasOwnProperty.call(obj, key)) {
                    // Check on toggle being boolean otherwise the component will be in undetermined state
                    if (["secondLevelTimingParameters",
                        "thirdLevelTimingParameters"
                    ].includes(this.mapper[key])) {
                        newObj[this.mapper[key]] = obj[key] === 'true' || obj[key] === "1"
                    } else {
                        newObj[this.mapper[key]] = obj[key];
                    }
                }
            }
            newObj["isManualSkullPressureTransmission"] = true
            return newObj;
        },
        exportState(format, filename) {
            const state = {
                "Pressure Amplitude [MPa]": +this.ppp_mpa,
                "Peak Negative Pressure [MPa]": +this.pnp_mpa,
                "Isppa [W/cm2]": +this.isppa,
                "MI": +this.MI,
                "Fundamental Frequency [MHz]": +this.frequency_MHz,
                "Pulse Duration [ms]": +this.pulse_duration_ms,
                "Ramp Duration [ms]": +this.pulse_ramp_duration_ms,
                "Pulse Ramp Shape": this.pulse_ramp_shape,
                "Pulse Repetition Interval [ms]": +this.pulse_repetition_interval_ms,
                "Pulse Train Duration [ms]": +this.pulse_train_duration_ms,
                "Pulse Train Ramp Duration [ms]": +this.pulse_train_ramp_duration_ms,
                "Pulse Train Ramp Shape": this.pulse_train_ramp_shape,
                "Pulse Train Repetition Interval [ms]": +this.pulse_train_repetition_interval_ms,
                "Pulse Train Repetition Duration [ms]": +this.pulse_train_repetition_duration_ms,
                "Pulse Train Repetition Ramp Duration [ms]": +this.pulse_train_repetition_ramp_duration_ms,
                "Pulse Train Repetition Ramp Shape": this.pulse_train_repetition_ramp_shape,
                "Density of brain [kg/m3]": +this.rho_kg_m3,
                "Speed Of Sound of Brain [m/s]": +this.c_m_s,
                "Diameter of transducer [mm]": +this.diameter_of_transducer_mm,
                "Focal depth [mm]": +this.focal_depth_mm,
                "Skull thickness [mm]": +this.thickness_of_skull_mm,
                "Scalp thickness [mm]": +this.thickness_of_scalp_mm,
                "Coupling thickness [mm]": +this.coupling_thickness_mm,
                "Skull pressure transmission [-]": this.isManualSkullPressureTransmission
                    ? +this.skull_pressure_transmission_manual : +this.skull_pressure_transmission,
                "Brain attenuation coefficient [db/cm/MHz]": +this.attenuationCoefficient,
                "Brain absorption fraction [-]": +this.absorptionFraction,
                "Pulse Train is enabled": this.secondLevelTimingParameters,
                "Pulse Train Repetition is enabled": this.thirdLevelTimingParameters
            }

            if (format === 'json') {
                this.writeToFile(JSON.stringify(state, null, 2), format, 'application/json', filename)
            }

            if (format === 'yml') {
                const yamlString = this.dumpToYAML(state, '\r\n')
                this.writeToFile(yamlString, format, 'application/x-yaml', filename)
            }
        },
        dumpToYAML(obj, lineEnding = '\n') {
            return Object.entries(obj)
                .map(([key, value]) => `${key}: ${value}`)
                .join(lineEnding);
        },
        writeToFile(jsonString, format, fType, filename) {
          const blob = new Blob([jsonString], { type: fType })
          const url = URL.createObjectURL(blob)
          const a = document.createElement('a')

          a.href = url;
          a.download = filename+'.'+format
          document.body.appendChild(a);

          a.click();
          document.body.removeChild(a);
          URL.revokeObjectURL(url);
        },
        switchToManualSkullPressureTransmission (bool) {
            this.skull_pressure_transmission_manual = this.skull_pressure_transmission
            this.isManualSkullPressureTransmission = bool
            this.updateByPr()
        },
        calculate_two_alphaI_rhoCt (attenuation_np_cm, isppa, density, specific_heat) {
            return 2 * attenuation_np_cm * isppa / density / specific_heat
        },
        calculate_energy_deposition(table) {
            let energy_deposition

            // Define variables based on the table value
            let isppa = table === 'freefield' ? this.isppa : this.derated_isppa
            let ispta_pulse_train = table === 'freefield' ? this.ispta_pulse_train : this.derated_ispta_pulse_train
            let ispta_pulse_train_repetition = table === 'freefield' ? this.ispta_pulse_train_repetition : this.derated_ispta_pulse_train_repetition

            // Determine energy deposition based on pulse type
            if (this.pulseOnly) {
                energy_deposition = this.round(isppa * this.pulse_duration_ms)
            } else if (this.pulseAndTrainOnly) {
                energy_deposition = this.round(ispta_pulse_train * this.pulse_train_duration_ms)
            } else {
                energy_deposition = this.round(ispta_pulse_train_repetition * this.pulse_train_repetition_duration_ms)
            }

            return +(energy_deposition * (10 ** -3)).toFixed(3);
        },
        createTable(doc, configObject, startY) {
            autoTable(doc, {
                startY: startY,
                head: [configObject.columns.map(head => head.title)],
                body: [
                    Object.values(configObject.rows[0]).map(v => v.toString())
                ],
                styles: {
                    // fillColor: [51, 51, 51],
                    lineColor: 240,
                    lineWidth: 1,
                    fontSize: 7
                },
                columnStyles: {
                    col1: {fillColor: false},
                    col2: {fillColor: false},
                    col3: {fillColor: false},
                    col4: {fillColor: false},
                    col5: {fillColor: false},
                    col6: {fillColor: false},
                },
                margin: {
                    top: 80
                }
            });
        },
        async generatePDF () {
            const doc = new jspdf('l', "pt");
            const tableHeight = 70
            doc.text("TUS-calculator results", 40, 50);

            doc.setFontSize(12)

            let startY = 80
            doc.text('Free Field Values at the Focus', 40, startY-5);
            this.createTable(doc, this.tableConfigurations.notderated, startY);

            startY += tableHeight
            doc.text('Conservatively Derated Values at the Focus', 40, startY-5);
            this.createTable(doc, this.tableConfigurations.derated, startY);

            startY += tableHeight
            doc.text('Timing', 40, startY-5);
            this.createTable(doc, this.tableConfigurations.timing, startY);

            startY += tableHeight
            doc.text('Assumptions', 40, startY-5);
            this.createTable(doc, this.tableConfigurations.assumptions, startY);

            startY += tableHeight
            doc.text('Derating values', 40, startY-5);
            this.createTable(doc, this.tableConfigurations.derating_values, startY);

            startY += 2 * tableHeight

            const disclaimerMessages = [
                'This calculator is solely developed to estimate derived quantities like ultrasound intensity and mechanical index, which have consequences for both safety and efficacy of ultrasound protocols.',
                'It does not account for individual differences in anatomy, and is not intended to, and should not be used to, replace an individualized acoustic simulation.',
                'The calculator is not intended to be and should not be used as a medical device as defined in the Medical Device Regulation (Regulation (EU) 2017/745).',
                'Use of the calculator is at one’s own risk: the calculator is provided “as is”, without warranty of any kind, express or implied, including but not',
                'limited to the warranties of merchantability, safety, fitness for a particular purpose and noninfringement of any rights. In no event shall the creators,',
                'authors or copyright holders be liable for any claim, damages or other liability, arising from, out of or in connection with the calculator or the use of',
                'the calculator or data generated with or with the help of the calculator.']
            doc.setFontSize(12)

            let linePosition = startY
            doc.text('Disclaimer', 40, linePosition - 20);
            doc.setFontSize(8)

            for (const message of disclaimerMessages) {
                doc.text(message, 40, linePosition);
                linePosition += 10
            }

            await doc.save("charts.pdf", {
                returnPromise: true
            });
        },
        async generateZIP () {
            const zip = new JSZip();
            zip.file("TUS-calculator-config.csv", this.generateParameterConfigCSVstring());
            zip.file("TUS-calculator-results.csv", this.generateCSVstring());
            zip.generateAsync({type:"blob"}).then(function(content) {
                FileSaver.saveAs(content, 'download.zip');
            });
        },
        generateCSVstring() {
            return this.tableConfigurations.notderated.columns.map(item => item.title).join(',')
                .concat(',')
                .concat(this.tableConfigurations.derated.columns.map(item => item.title).join(','))
                .concat('\n')
                .concat(Object.values(this.tableConfigurations.notderated.rows[0]).join(','))
                .concat(',')
                .concat(Object.values(this.tableConfigurations.derated.rows[0]).join(','))
                .concat('\n')
        },
        generateParameterConfigCSVstring() {
            return this.tableConfigurations.notderated.columns.map(item => item.title).join(',')
                .concat(this.tableConfigurations.derated.columns.map(item => item.title).join(','))
                .concat(this.tableConfigurations.timing.columns.map(item => item.title).join(','))
                .concat(this.tableConfigurations.assumptions.columns.map(item => item.title).join(','))
                .concat(this.tableConfigurations.derating_values.columns.map(item => item.title).join(','))
                .concat('\n')
                .concat(Object.values(this.tableConfigurations.notderated.rows[0]).join(','))
                .concat(Object.values(this.tableConfigurations.derated.rows[0]).join(','))
                .concat(Object.values(this.tableConfigurations.timing.rows[0]).join(','))
                .concat(Object.values(this.tableConfigurations.assumptions.rows[0]).join(','))
                .concat(Object.values(this.tableConfigurations.derating_values.rows[0]).join(','))
                .concat('\n')
        },
        async exportResults(format) {
            format === 'pdf'
                ? await this.generatePDF()
                : await this.generateZIP()
        },
        func(freq) {
          return Number.parseFloat((this.limits.MI * Math.sqrt(freq)).toFixed(2))
        },
        funcISPPA() {
          return Number.parseFloat(Math.sqrt(this.limits.ISPPA * 2 * this.z_Rayl) / 10).toFixed(2)
        },
        generatePrMIData() {
          const data = []
          const max_freq = this.frequency_MHz <= 3 ? 3 : +this.frequency_MHz * 2
          const step = this.frequency_MHz <= 3 ? 0.05 : 0.1
          for (let i = 0; i <= max_freq; i += step) {
            data.push([i, this.func(i)]);
          }
          return data
        },
        generatePrISPPAData() {
          const data = []
          const max_freq = this.frequency_MHz <= 3 ? 3 : +this.frequency_MHz * 2
          for (let i = 0; i <= max_freq; i += 0.05) {
            data.push([i, this.funcISPPA()]);
          }
          return data
        },
        getMyValue() {
          return this.derate ? [[ this.frequency_MHz, this.derated_ppp_mpa ]] : null
        },
        getMyNotDeratedValue() {
          return [[ this.frequency_MHz, this.ppp_mpa ]]
        },
        round(val) {
            return Number.parseFloat(Number.parseFloat(val).toFixed(2))
        },
        restartWorker() {
          if (this.isppaWorker) {
              this.isppaWorker.destroy()
          }
        },
        showRecalculateButton() {
            this.parametersChanged = !!(this.primary_parameters_valid && this.time_parameters_valid && this.time_parameters_gt_0);
            this.activateCanvasRefreshButton()
        },
        recalculate() {
          if (this.primary_parameters_valid && this.time_parameters_valid) {
              this.updateByPr()
              this.getAverageAcousticPower()
          }
        },
        initAcousticPowerWorker() {
            this.avgAcPowWorker = new AcPowWorker({type: 'module'})
            this.avgAcPowWorker.onmessage = (evt) => {
                this.average_acoustic_power = evt.data.average_acoustic_power
                                            Loading.hide("average_acoustic_power")

            }
        },
        initWorker() {
            this.initAcousticPowerWorker()
            if (typeof Worker !== "undefined") {
                if (!this.isppaWorker) {
                    this.isppaWorker = new Worker({type: 'module'})
                    this.isppaWorker.onmessage = (evt) => {
                        if (['isppa', 'isppaAfterPampl'].includes(evt.data.job)) {
                            if(evt.data.job === 'isppa') this.isppa = evt.data.isppa

                            this.derated_isppa = evt.data.derated_isppa
                            if (this.pulseAndTrainOnly || this.allTimeTogglesOn) {
                                this.ispta_pulse_train = evt.data.ispta_pulse_train
                                this.derated_ispta_pulse_train = evt.data.derated_ispta_pulse_train

                            } else {
                                this.ispta_pulse_train = 0
                                this.derated_ispta_pulse_train = 0
                            }

                            if (this.allTimeTogglesOn) {
                                this.ispta_pulse_train_repetition = evt.data.ispta_pulse_train_repetition
                                this.derated_ispta_pulse_train_repetition = evt.data.derated_ispta_pulse_train_repetition
                            } else {
                                this.ispta_pulse_train_repetition = 0
                                this.derated_ispta_pulse_train_repetition = 0
                            }
                            Loading.hide("isppa")

                        } else if (evt.data.job === 'pampl-result') {
                            this.ppp_mpa_unformatted = evt.data.ppp_mpa
                            this.derated_ppp_mpa_unformatted = evt.data.derated_ppp_mpa
                            this.ppp_mpa = +(this.ppp_mpa_unformatted.toFixed(2))
                            this.pnp_mpa = this.ppp_mpa
                            this.derated_ppp_mpa = +(this.derated_ppp_mpa_unformatted.toFixed(2))
                            this.derated_pnp_mpa = this.derated_ppp_mpa

                            this.calculateMI()
                            this.derated_MI = this.round(this.derated_pnp_mpa / Math.sqrt(this.frequency_MHz))
                            Loading.hide("pampl")
                            this.getIsppa(this.ppp_mpa_unformatted, this.derated_ppp_mpa_unformatted, 'isppaAfterPampl')
                        }
                        console.info("isppaWorker message processed")
                        setTimeout(()=>{
                            this.parametersChanged = false
                        },200)
                    }
                }
            }
        },
        getAverageAcousticPower(){
            if (typeof AcPowWorker !== "undefined") {
                let message = {
                    acoustic_power: this.acoustic_power,
                    pulseOnly: this.pulseOnly,
                    pulseAndTrainOnly: this.pulseAndTrainOnly,
                    allTimeTogglesOn: this.allTimeTogglesOn,
                    job: 'average_acoustic_power',
                    isppa: +this.isppa,
                    derated_isppa: this.derated_isppa,
                    z_Rayl: this.z_Rayl,
                    pulseDur: this.pulse_duration_ms,
                    rampDur: this.pulse_ramp_duration_ms,
                    d_reas_coeff: this.d_reas_coeff,
                    brain_pressure_transmission: this.brain_pressure_transmission,
                    pulseRepetitionInterval: this.pulse_repetition_interval_ms,
                    pulseTrainDuration: this.pulse_train_duration_ms,
                    pulseTrainRampDuration: this.pulse_train_ramp_duration_ms,
                    pulseTrainRepetitionInterval: this.pulse_train_repetition_interval_ms,
                    nPulses: this.pulse_train_duration_ms / this.pulse_repetition_interval_ms,
                    nTrains: this.pulse_train_repetition_duration_ms / this.pulse_train_repetition_interval_ms,
                    time_parameters_valid: this.time_parameters_valid,
                    pulse_ramp_shape: this.pulse_ramp_shape,
                    pulse_ramp_duration_ms: this.pulse_ramp_duration_ms,
                    showPulseTrain: this.secondLevelTimingParameters,
                    showPulseTrainRepetition: this.thirdLevelTimingParameters,
                    ppp_mpa: this.ppp_mpa_unformatted ? this.ppp_mpa_unformatted : this.ppp_mpa,
                    derated_ppp_mpa: this.derated_ppp_mpa_unformatted ? this.derated_ppp_mpa_unformatted : this.derated_ppp_mpa,
                    frequency_MHz: this.frequency_MHz,
                    pulseTrainRepetitionDuration: this.pulse_train_repetition_duration_ms,
                    pulseTrainRampShape: this.pulse_train_ramp_shape,
                    pulseTrainRepetitionRampShape: this.pulse_train_repetition_ramp_shape,
                    pulseTrainRepetitionRampDuration: this.pulse_train_repetition_ramp_duration_ms,
                }

                message = this.timeParametersByToggles(message)
                this.loading_processes.push('acousticPower')
                Loading.show({
                    group: "average_acoustic_power",
                    spinner: QSpinnerGears,
                    message: 'Calculating acoustic power. Please wait...'
                    // other props
                })

                this.avgAcPowWorker.postMessage(message);
            } else {
              // Fallback mechanism for browsers without web worker support
              console.error('Your browser does not support web workers')
            }
        },
        getPampl() {
            if (typeof Worker !== "undefined") {
                let message = {
                    pulseOnly: this.pulseOnly,
                    pulseAndTrainOnly: this.pulseAndTrainOnly,
                    allTimeTogglesOn: this.allTimeTogglesOn,
                    job: 'pampl',
                    isppa: +this.isppa,
                    derated_isppa: this.derated_isppa,
                    z_Rayl: this.z_Rayl,
                    pulseDur: this.pulse_duration_ms,
                    rampDur: this.pulse_ramp_duration_ms,
                    d_reas_coeff: this.d_reas_coeff,
                    brain_pressure_transmission: this.brain_pressure_transmission,
                    pulseRepetitionInterval: this.pulse_repetition_interval_ms,
                    pulseTrainDuration: this.pulse_train_duration_ms,
                    pulseTrainRampDuration: this.pulse_train_ramp_duration_ms,
                    pulseTrainRepetitionInterval: this.pulse_train_repetition_interval_ms,
                    nPulses: this.pulse_train_duration_ms / this.pulse_repetition_interval_ms,
                    nTrains: this.pulse_train_repetition_duration_ms / this.pulse_train_repetition_interval_ms,
                    time_parameters_valid: this.time_parameters_valid,
                    pulse_ramp_shape: this.pulse_ramp_shape,
                    pulse_ramp_duration_ms: this.pulse_ramp_duration_ms,
                    showPulseTrain: this.secondLevelTimingParameters,
                    showPulseTrainRepetition: this.thirdLevelTimingParameters,
                    ppp_mpa: this.ppp_mpa_unformatted ? this.ppp_mpa_unformatted : this.ppp_mpa,
                    derated_ppp_mpa: this.derated_ppp_mpa_unformatted ? this.derated_ppp_mpa_unformatted : this.derated_ppp_mpa,
                    frequency_MHz: this.frequency_MHz,
                    pulseTrainRepetitionDuration: this.pulse_train_repetition_duration_ms,
                    pulseTrainRampShape: this.pulse_train_ramp_shape,
                    pulseTrainRepetitionRampShape: this.pulse_train_repetition_ramp_shape,
                    pulseTrainRepetitionRampDuration: this.pulse_train_repetition_ramp_duration_ms,
                }

                message = this.timeParametersByToggles(message)
                this.loading_processes.push('pampl')
                Loading.show({
                    group: "pampl",
                    spinner: QSpinnerGears,
                    message: 'Calculating pressure amplitude. Please wait...'
                    // other props
                })

                this.isppaWorker.postMessage(message);
            } else {
              // Fallback mechanism for browsers without web worker support
              console.error('Your browser does not support web workers')
            }
        },
        timeParametersByToggles(message) {
            if (this.time_parameters_valid) {
                    // Only pulse toggle on:
                    // Pulse Repetition Interval, Pulse Train Duration, Pulse Train Repetition Interval, Pulse train repetition duration equal the pulse duration.
                    // Pulse Train Ramp duration and Pulse Train Repetition Ramp duration are zero.
                    // Hide both Ispta's from results (non-derated and derated) and don't do the calculations to save time.
                    if (this.pulseOnly) {
                        message.pulseRepetitionInterval = this.pulse_duration_ms
                        message.pulseTrainDuration = this.pulse_duration_ms
                        message.pulseTrainRepetitionInterval = this.pulse_duration_ms
                        message.pulseTrainDuration = this.pulse_duration_ms
                        message.pulseTrainRampDuration = 0
                        message.pulseTrainRepetitionRampDuration = 0
                        message.label = "Isppa"
                    }

                    else if (this.pulseAndTrainOnly) {
                        // Only pulse and pulse train toggle on:
                        // Pulse Train Repetition Interval, Pulse train repetition duration equal Pulse Train Duration.
                        //  Pulse Train Repetition Ramp duration is zero.
                        // Hide Ispta of pulse train repetition from results (non-derated and derated) and don't do the calculation to save time. Potentially add warning that calculation may take a time.
                        message.pulseTrainRepetitionInterval = this.pulse_train_duration_ms
                        message.pulseTrainRepetitionDuration = this.pulse_train_duration_ms
                        message.pulseTrainRepetitionRampDuration = 0
                        message.label = "Isppa and Ispta of pulse train"
                    } else {
                        message.label = "Isppa, Ispta of pulse train and Ispta of pulse train repetition"
                    }
                }
            return message
        },
        getIsppa(ppp_mpa, derated_ppp_mpa, job) {
            if (typeof Worker !== "undefined") {

                let message = {
                    job: job ? job : 'isppa',
                    pulseOnly: this.pulseOnly,
                    pulseAndTrainOnly: this.pulseAndTrainOnly,
                    allTimeTogglesOn: this.allTimeTogglesOn,
                    isppa: +this.isppa,
                    derated_isppa: this.derated_isppa,
                    z_Rayl: this.z_Rayl,
                    pulseDur: this.pulse_duration_ms,
                    rampDur: this.pulse_ramp_duration_ms,
                    pulseRepetitionInterval: this.pulse_repetition_interval_ms,
                    pulseTrainDuration: this.pulse_train_duration_ms,
                    pulseTrainRampDuration: this.pulse_train_ramp_duration_ms,
                    pulseTrainRepetitionInterval: this.pulse_train_repetition_interval_ms,
                    nPulses: this.pulse_train_duration_ms / this.pulse_repetition_interval_ms,
                    nTrains: this.pulse_train_repetition_duration_ms / this.pulse_train_repetition_interval_ms,
                    time_parameters_valid: this.time_parameters_valid,
                    pulse_ramp_shape: this.pulse_ramp_shape,
                    pulse_ramp_duration_ms: this.pulse_ramp_duration_ms,
                    showPulseTrain: this.secondLevelTimingParameters,
                    showPulseTrainRepetition: this.thirdLevelTimingParameters,
                    ppp_mpa: ppp_mpa,
                    derated_ppp_mpa: derated_ppp_mpa,
                    frequency_MHz: this.frequency_MHz,
                    pulseTrainRepetitionDuration: +this.pulse_train_repetition_duration_ms,
                    pulseTrainRampShape: this.pulse_train_ramp_shape,
                    pulseTrainRepetitionRampShape: this.pulse_train_repetition_ramp_shape,
                    pulseTrainRepetitionRampDuration: this.pulse_train_repetition_ramp_duration_ms,
                }

                message = this.timeParametersByToggles(message)

                this.loading_processes.push('isppa')
                Loading.show({
                    group: "isppa",
                    spinner: QSpinnerGears,
                    message: `Calculating ${message.label}...`
                    // other props
                })
                this.isppaWorker.postMessage(message);
            } else {
              // Fallback mechanism for browsers without web worker support
              console.error('Your browser does not support web workers')
            }
        },
        calculateMI() {
            //  Mechanical index, standardized indicator related to the potential for mechanical bioeffects, specifically cavitation.
            //  The peak-rarefactional pressure derated using an attenuation coefficient of 0.3 dB/cm/MHz is used in this expression,
            //  as this is the formal definition of MI.
            //  Please see the ITRUSST Standardized Reporting Consensus for further explanation.
            const attenuation_coefficient = 0.3
            const attenuation_factor =  attenuation_coefficient * this.depth_cm * this.frequency_MHz
            const peak_rarefractional_pressure = this.pnp_mpa * (Math.exp( -attenuation_factor / 8.686))
            this.MI = this.round(peak_rarefractional_pressure / Math.sqrt(this.frequency_MHz))
        },
        updateByPr() {
            if (!this.primary_parameters_valid && !this.time_parameters_valid) return
            this.ppp_mpa_unformatted = this.ppp_mpa
            this.pnp_mpa = this.ppp_mpa
            this.calculateMI()
            this.derated_ppp_mpa_unformatted = this.ppp_mpa * this.d_reas_coeff * this.brain_pressure_transmission
            this.derated_ppp_mpa = this.round(this.derated_ppp_mpa_unformatted)
            this.derated_pnp_mpa = this.round(this.derated_ppp_mpa_unformatted)
            this.derated_MI = this.round(this.derated_pnp_mpa/Math.sqrt(this.frequency_MHz))
            this.getIsppa(this.ppp_mpa, this.derated_ppp_mpa_unformatted)
        },
        updateByIsppa() {
            if (!this.primary_parameters_valid && !this.time_parameters_valid) return
            if (this.isppa > 0 && this.isppa !== "") this.getPampl()
        },
        updateByMI() {
            if (!this.primary_parameters_valid && !this.time_parameters_valid) return
            const attenuation_coefficient = 0.3;
            const attenuation_factor = attenuation_coefficient * this.depth_cm * this.frequency_MHz;
            // Calculate the peak rarefactional pressure (pnp_mpa) from the MI
            const peak_rarefractional_pressure = this.MI * Math.sqrt(this.frequency_MHz);

            // Apply the attenuation factor to calculate the original pnp_mpa
            this.ppp_mpa_unformatted = peak_rarefractional_pressure / Math.exp(-attenuation_factor / 8.686);
            this.ppp_mpa = this.round(this.ppp_mpa_unformatted)
            this.pnp_mpa = this.round(this.ppp_mpa_unformatted)
            this.derated_MI = this.round(this.MI * this.d_reas_coeff * this.brain_pressure_transmission)
            this.derated_ppp_mpa_unformatted = this.derated_MI * Math.sqrt(this.frequency_MHz)
            this.derated_ppp_mpa = this.derated_ppp_mpa_unformatted
            this.derated_pnp_mpa = this.derated_ppp_mpa_unformatted
            this.getIsppa(this.ppp_mpa, this.derated_ppp_mpa)
        },
        updateByElectricalPower() {
            this.acoustic_power = +((this.transducer_efficiency / 100) * this.electrical_power).toFixed(2)
            this.getAverageAcousticPower()
        },
        updateByTransducerEfficiency() {
            this.acoustic_power = +((this.transducer_efficiency / 100) * this.electrical_power).toFixed(2)
            this.getAverageAcousticPower()
        },
        updateByAcousticPower() {
            this.electrical_power = +(this.acoustic_power / (this.transducer_efficiency / 100)).toFixed(2)
            this.getAverageAcousticPower()
        }
  }
})