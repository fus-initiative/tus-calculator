export function findRangeInArray(target, arr) : number[] {
  // Sort the array in ascending order
  arr.sort((a, b) => a - b)
  const arrayLength = arr.length - 1

  for (let i = 0; i < arrayLength; i++) {
    if (arr[i] <= target && target <= arr[i + 1]) {
      return [arr[i], arr[i + 1]]
    }
  }
  return null // If the value is not within any range in the array
}


export function findMostConservativeValue(target, array): number {
    // take conservative value for max number in Array if the target is larger.
    const maxValue = Math.max(...array)
    const minValue = Math.min(...array)
    target = target > maxValue ? maxValue : target
    target = target < minValue ? minValue : target
    if (array.includes(target)) {
        return target
    }
    return Math.min(...findRangeInArray(target, array))
}

export const ultrasoundBeamDiameterArray: number[] = [
    0, 5, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100
]

export const ultrasoundFrequencykHZArray: number[] = [
    0, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 1100, 1200, 1300, 1400, 1500
]

export const lookupTable: { [key: string]: number } = {
    // 'US frequency, Us beam diameter mm': Pressure
    '0,0': 74,
    '0,5': 74,
    '0,10': 73,
    '0,20': 71,
    '0,30': 70,
    '0,40': 69,
    '0,50': 68,
    '0,60': 67,
    '0,70': 66,
    '0,80': 66,
    '0,90': 65,
    '0,100': 64,

    '100,0': 74,
    '100,5': 74,
    '100,10': 73,
    '100,20': 71,
    '100,30': 70,
    '100,40': 69,
    '100,50': 68,
    '100,60': 67,
    '100,70': 66,
    '100,80': 66,
    '100,90': 65,
    '100,100': 64,

    '200,0': 78,
    '200,5': 78,
    '200,10': 77,
    '200,20': 75,
    '200,30': 74,
    '200,40': 73,
    '200,50': 72,
    '200,60': 72,
    '200,70': 71,
    '200,80': 71,
    '200,90': 70,
    '200,100': 70,

    '300,0': 78,
    '300,5': 77,
    '300,10': 76,
    '300,20': 75,
    '300,30': 73,
    '300,40': 72,
    '300,50': 71,
    '300,60': 71,
    '300,70': 70,
    '300,80': 69,
    '300,90': 69,
    '300,100': 68,

    '400,0': 76,
    '400,5': 74,
    '400,10': 72,
    '400,20': 69,
    '400,30': 67,
    '400,40': 65,
    '400,50': 64,
    '400,60': 63,
    '400,70': 62,
    '400,80': 62,
    '400,90': 61,
    '400,100': 61,

    '500,0': 70,
    '500,5': 68,
    '500,10': 65,
    '500,20': 63,
    '500,30': 62,
    '500,40': 61,
    '500,50': 60,
    '500,60': 59,
    '500,70': 59,
    '500,80': 58,
    '500,90': 58,
    '500,100': 57,

    '600,0': 65,
    '600,5': 64,
    '600,10': 63,
    '600,20': 61,
    '600,30': 60,
    '600,40': 59,
    '600,50': 58,
    '600,60': 57,
    '600,70': 57,
    '600,80': 56,
    '600,90': 56,
    '600,100': 56,

    '700,0': 65,
    '700,5': 63,
    '700,10': 61,
    '700,20': 60,
    '700,30': 58,
    '700,40': 57,
    '700,50': 57,
    '700,60': 56,
    '700,70': 55,
    '700,80': 55,
    '700,90': 54,
    '700,100': 54,

    '800,0': 64,
    '800,5': 61,
    '800,10': 60,
    '800,20': 58,
    '800,30': 57,
    '800,40': 56,
    '800,50': 55,
    '800,60': 54,
    '800,70': 53,
    '800,80': 53,
    '800,90': 52,
    '800,100': 52,

    '900,0': 62,
    '900,5': 58,
    '900,10': 57,
    '900,20': 56,
    '900,30': 55,
    '900,40': 54,
    '900,50': 53,
    '900,60': 52,
    '900,70': 51,
    '900,80': 51,
    '900,90': 50,
    '900,100': 50,

    '1000,0': 59,
    '1000,5': 56,
    '1000,10': 55,
    '1000,20': 54,
    '1000,30': 53,
    '1000,40': 52,
    '1000,50': 51,
    '1000,60': 51,
    '1000,70': 50,
    '1000,80': 49,
    '1000,90': 49,
    '1000,100': 48,

    '1100,0': 55,
    '1100,5': 54,
    '1100,10': 54,
    '1100,20': 53,
    '1100,30': 52,
    '1100,40': 51,
    '1100,50': 50,
    '1100,60': 49,
    '1100,70': 48,
    '1100,80': 48,
    '1100,90': 47,
    '1100,100': 47,

    '1200,0': 55,
    '1200,5': 53,
    '1200,10': 52,
    '1200,20': 51,
    '1200,30': 50,
    '1200,40': 49,
    '1200,50': 48,
    '1200,60': 48,
    '1200,70': 47,
    '1200,80': 46,
    '1200,90': 45,
    '1200,100': 45,

    '1300,0': 54,
    '1300,5': 52,
    '1300,10': 51,
    '1300,20': 50,
    '1300,30': 49,
    '1300,40': 48,
    '1300,50': 47,
    '1300,60': 46,
    '1300,70': 45,
    '1300,80': 45,
    '1300,90': 44,
    '1300,100': 43,

    '1400,0': 52,
    '1400,5': 51,
    '1400,10': 50,
    '1400,20': 49,
    '1400,30': 48,
    '1400,40': 46,
    '1400,50': 46,
    '1400,60': 45,
    '1400,70': 44,
    '1400,80': 43,
    '1400,90': 42,
    '1400,100': 42,

    '1500,0': 50,
    '1500,5': 50,
    '1500,10': 49,
    '1500,20': 48,
    '1500,30': 46,
    '1500,40': 45,
    '1500,50': 44,
    '1500,60': 43,
    '1500,70': 43,
    '1500,80': 42,
    '1500,90': 41,
    '1500,100': 40,
}
