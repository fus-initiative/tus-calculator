import './assets/main.css'
import { createApp } from 'vue/dist/vue.esm-bundler'
import { Quasar, Notify, Dialog, Loading } from 'quasar'
import '@quasar/extras/material-icons/material-icons.css'
import 'quasar/src/css/index.sass'
import App from './App.vue'
import { createPinia } from 'pinia'

const app = createApp(App)
app.use(createPinia())
app.use(Quasar, {
  plugins: {
    Notify,
    Dialog,
    Loading
  },
  config: {
    loading: {} /* look at QuasarConfOptions from the API card */
  }
})

app.mount('#app')