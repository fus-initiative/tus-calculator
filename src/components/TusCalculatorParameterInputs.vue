<script setup lang="ts">
import {storeToRefs} from "pinia"
import {useCalculatorStore} from '../stores/calculator.store'
import {computed, ref} from "vue"
import TimingParameters from "./TimingParameters.vue"
import TimingDocs from "./TimingDocs.vue"
import ReferencePopupProxy from "./PopupProxyLiteratureReference.vue"
import ThicknessDocs from "./DialogThicknessDocs.vue"
import PppPnpDocs from "./DialogPeakPressureGraphDocs.vue"
import TempRiseEquationDialog from "./TempRiseEquationDialog.vue"

const calculatorStore = useCalculatorStore()
const {
  ppp_mpa,
  pnp_mpa,
  isppa,
  MI,
  diameter_of_transducer_mm,
  focal_depth_mm,
  thickness_of_skull_mm,
  thickness_of_scalp_mm,
  coupling_thickness_mm,
  frequency_MHz,
  rho_kg_m3,
  c_m_s,
  derate,
  isManualSkullPressureTransmission,
  isDarkModeGlobal,
  timingParameters,
  att_coef_abs,
  skull_pressure_transmission,
  skull_pressure_transmission_manual,
  brain_pressure_transmission,
  coeff_dB_cm_MHz,
  parametersChanged,
  time_parameters_valid,
  acoustic_power,
  electrical_power,
  transducer_efficiency
} = storeToRefs(calculatorStore)

const deratingParameters = ref(true)
const confirm = ref(false)
const file = ref(null)
const canUpload = computed(() => file.value !== null)

const toggleMessage = 'Please see ITRUSST Standardized Reporting paper for information about situations in which PPP and PNP may differ. Currently, this calculator cannot account for all the effects of unequal PPP and PNP, and therefore the option to use unequal PPP and PNP is unavailable.'
const attenuationCoeffMessage = `The attenuation coefficient reflects the decrease in ultrasound pressure as it travels through a medium, expressed in decibels per unit distance (in cm), per unit fundamental frequency (in MHz). The attenuation coefficient depends on the medium; for example, it is higher for bone compared to soft tissues, indicating that bone attenuates ultrasound more than soft tissues. The default is based on estimates reported in the literature (see reference) and is set to a relatively low value. Consequently, the pressure decrease is relatively small and the Isspa estimate is relatively high. This default was chosen to represent a the worst-case scenario in terms of safety. Please note that a different default attenuation coefficient value is used for estimating temperature rise.`
const rules = [val => !!val || 'This field is required', val => val > 0 || 'Value must be > 0']
const minimumValueFocalDepth = computed(() => +coupling_thickness_mm.value + +thickness_of_scalp_mm.value + +thickness_of_skull_mm.value)

const fileType = computed(() => file.value.type)
const isYaml = computed(() => fileType.value === "application/x-yaml"
    || file.value.name.endsWith(".yml")
    || file.value.name.endsWith(".yaml"))
const isJson = computed(() => fileType.value === "application/json")
const isppaMessage = `Spatial-peak pulse average intensity; the Isppa is calculated for an individual pulse taking pulse level ramping into account. However, when ramping is applied at the pulse train or pulse train repetition level, the pulse amplitude and therefore Isspa is not consistent over time. The Isppa reported here is the maximum value which corresponds to the highest amplitude pulses, which occur after the on-ramp is completed and before the off-ramp begins of pulse train and/or pulse train repetition level ramping.`
const importSuccessShow = ref(false)
const settingsExportConfirmationDialog = ref(false)
const filename = ref('data')

function resetParameters() {
  const previousDarkModeGlobalValue = isDarkModeGlobal.value
  calculatorStore.$reset()
  calculatorStore.restartWorker()
  calculatorStore.$patch({
    confirmConditionsOfUse: true,
    isDarkModeGlobal: previousDarkModeGlobalValue
  })
  calculatorStore.initWorker()
  calculatorStore.recalculate()
}

function togglePPP_PNP() {
  if (calculatorStore.isEqualPPPPNP) {
    confirm.value = true
  } else {
    calculatorStore.isEqualPPPPNP = !calculatorStore.isEqualPPPPNP
    pnp_mpa.value = ppp_mpa.value
  }
}

function importSuccess() {
      importSuccessShow.value = true
      setTimeout(() => {
        importSuccessShow.value = false
        file.value = null
      }, 1000)
}

function importToState(jsonObject) {
    try {
      const newState = calculatorStore.mapToStateKeys(jsonObject)

      calculatorStore.$patch(newState)
      importSuccess()
    } catch {
        console.error('import failed')
    }
}

function downloadDialog() {
  settingsExportConfirmationDialog.value = true
}

function parseSimpleYAML(yamlString) {
  const lines = yamlString.split(/\r?\n/g)
  const result = {}
  lines.forEach(line => {
    // Ignore empty lines or lines that start with a comment
    if (line.trim() === '' || line.trim().startsWith('#')) {
      return
    }

    // Split the line by the first colon
    const [key, value] = line.split(/:(.*)/).map(part => part.trim())

    // If both key and value exist, add to the result object
    if (key && value !== undefined) {
        result[key] = value
    }
  })
  return result
}

const blurActiveInput = () => {
  const activeElement = document.activeElement;
  if (activeElement && activeElement.tagName === 'INPUT') {
    activeElement.blur()
  }
};

let debounceTimeout = null // Timeout reference for debounce
let interval = null // Timeout reference for interval
const loading = ref(false) // Tracks spinner visibility
const loadingValue = ref(0)

let totalTime = 2000
let intervalTime = 100 // Time between each count

function executeCalculation(val, calcType) {
  if (debounceTimeout) {
    clearTimeout(debounceTimeout);
    clearInterval(interval)
    loadingValue.value = 0
  }

  loading.value = true;
  interval = setInterval(() => {
  loadingValue.value += 0.1;
    if (loadingValue.value > 2) {
      clearInterval(interval); // Stop the interval when count reaches 100
      loadingValue.value = 0
    }
  }, intervalTime);
      // Start debounce timer
  debounceTimeout = setTimeout(() => {
        // After debounce delay
    loading.value = false;
    if (val > 0 && focal_depth_mm.value >= minimumValueFocalDepth.value) {
      switch (calcType) {
        case 'acoustic_power':
          calculatorStore.updateByAcousticPower()
          break
        case 'transducer_efficiency':
          calculatorStore.updateByTransducerEfficiency()
          break
        case 'electrical_power':
          calculatorStore.updateByElectricalPower()
          break
        case 'isppa':
          calculatorStore.updateByIsppa()
          break
        case 'MI':
          calculatorStore.updateByMI()
          break
        default:
          calculatorStore.updateByPr()
          break
      }
      parametersChanged.value = false
      blurActiveInput()
    } else {
      console.error("empty value or 0 not allowed")
    }
  }, totalTime);
}
async function handleFileUpload() {
  if (!file.value) {
    console.error('No file selected');
    return;
  }

  try {
    const fileContent = await readFileAsText(file.value);
    parseAndImportFile(fileContent);
  } catch (error) {
    console.error('Error during file upload:', error.message);
  }
}

// Helper function to read file content as text
function readFileAsText(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader()
    reader.onload = (e) => resolve(e.target.result)
    reader.onerror = () => reject(new Error('Failed to read file'))
    reader.readAsText(file)
  })
}

// Helper function to handle file format parsing
function parseAndImportFile(content) {
  try {
    if (typeof content !== 'string') throw new Error('Invalid file content format')
    if (isJson.value) {
      importToState(JSON.parse(content))
    } else if (isYaml.value) {
      importToState(parseSimpleYAML(content))
    } else {
      throw new Error('Unsupported format, please use JSON or YAML')
    }
  } catch (error) {
    console.error('Error parsing file:', error.message)
  }
}
</script>
<template>
  <q-card
    flat
    style="min-width: 450px; max-width: 450px"
    :dark="isDarkModeGlobal"
  >
    <q-card-section :class="`q-gutter-xs q-pa-sm row justify-evenly ${isDarkModeGlobal ? 'bg-grey-10': 'bg-grey-1' }`">
      <q-fab
        round
        padding="sm"
        :dark="isDarkModeGlobal"
        @click="downloadDialog()"
        color="primary"
        icon="download"
        active-icon="download"
        label="Parameters"
        square
      >
        <q-tooltip>
          Export calculator settings
        </q-tooltip>
      </q-fab>
      <q-file
        clearable
        outlined
        filled
        dense
        :dark="isDarkModeGlobal"
        v-model="file"
        color="primary"
        label="Import yml or json"
        label-color="primary"
      >
        <template #prepend>
          <q-icon
            name="upload"
            color="primary"
          />
        </template>
        <template #append>
          <q-btn
            v-if="canUpload && !importSuccessShow"
            color="primary"
            icon="send"
            @click="handleFileUpload"
            dense
          >
            <q-tooltip>
              Import calculator state from file
            </q-tooltip>
          </q-btn>
          <q-icon
            v-if="importSuccessShow"
            name="check"
            color="green"
          />
        </template>
      </q-file>
      <q-btn
        fab-mini
        flat
        dense
        color="primary"
        icon="refresh"
        @click="resetParameters"
        label="All"
      >
        <q-tooltip>Reset all parameters to default values</q-tooltip>
      </q-btn>
    </q-card-section>

    <q-card-section class="q-gutter-xs q-pa-sm">
      <q-toolbar>
        <q-toolbar-title>
          Parameters
        </q-toolbar-title>
        <q-circular-progress
          :min="0"
          :max="1"
          :value="loadingValue"
          size="20px"
          color="primary"
          class="q-ma-md"
        />
      </q-toolbar>

      <q-input
        :dark="isDarkModeGlobal"
        filled
        dense
        for="ppp_mpa"
        label="Pressure Amplitude [MPa]"
        type="number"
        step="0.01"
        v-model="ppp_mpa"
        :rules="rules"
        hide-bottom-space

        @update:modelValue="executeCalculation(ppp_mpa, 'pr')"
      >
        <template #append>
          <ppp-pnp-docs />
        </template>
      </q-input>

      <q-dialog
        v-model="confirm"
        persistent
      >
        <q-card :dark="isDarkModeGlobal">
          <q-toolbar>
            <q-avatar
              icon="lock"
              color="primary"
              text-color="white"
            />

            <q-toolbar-title>Locked</q-toolbar-title>
          </q-toolbar>

          <q-card-section class="row items-center">
            <span class="q-ml-sm">{{ toggleMessage }}</span>
          </q-card-section>

          <q-card-actions align="right">
            <q-btn
              flat
              label="Ok"
              color="primary"
              v-close-popup
            />
          </q-card-actions>
        </q-card>
      </q-dialog>

      <q-dialog
        :dark="isDarkModeGlobal"
        v-model="settingsExportConfirmationDialog"
        transition-show="scale"
        transition-hide="scale"
      >
        <q-card
          :dark="isDarkModeGlobal"
          style="width: 600px"
        >
          <q-card-section>
            <div class="text-h6">
              <q-icon name="download" /> Download TUS-calculator settings
            </div>
          </q-card-section>

          <q-card-section
            class="text-center"
          >
            <q-input
              outlined
              :dark="isDarkModeGlobal"
              v-model="filename"
              label="Name"
            >
              <template #append>
                <q-btn
                  class="q-ma-sm q-pa-sm"
                  icon="download"
                  push
                  :label="`json`"
                  dense
                  color="primary"
                  @click="calculatorStore.exportState('json', filename)"
                >
                  <q-tooltip>
                    Download settings as JSON file
                  </q-tooltip>
                </q-btn>
                <q-btn
                  class="q-ma-sm q-pa-sm"
                  icon="download"
                  push
                  :label="`YAML`"
                  dense
                  color="primary"
                  @click="calculatorStore.exportState('yml', filename)"
                >
                  <q-tooltip>
                    Download settings as YAML file
                  </q-tooltip>
                </q-btn>
              </template>
            </q-input>
          </q-card-section>
          <q-card-actions
            align="right"
            class="text-teal"
          >
            <q-btn
              flat
              label="Close"
              color="primary"
              v-close-popup
            />
          </q-card-actions>
        </q-card>
      </q-dialog>
      <q-input
        :dark="isDarkModeGlobal"
        :readonly="calculatorStore.isEqualPPPPNP"
        :filled="!calculatorStore.isEqualPPPPNP"
        :outlined="calculatorStore.isEqualPPPPNP"
        dense
        for="ppp_mpa2"
        label="Peak Negative Pressure [MPa]"
        type="number"
        step="0.01"
        v-model="pnp_mpa"
        :rules="rules"
        hide-bottom-space

        @update:modelValue="executeCalculation(pnp_mpa, 'pr')"
      >
        <template #append>
          <q-icon
            :name="calculatorStore.isEqualPPPPNP? 'lock' : 'lock_open'"
            @click="togglePPP_PNP"
          >
            <q-tooltip>
              {{ calculatorStore.isEqualPPPPNP ? 'PPP and PNP are equal' : 'PPP and PNP can be different' }}
            </q-tooltip>
          </q-icon>
          <ppp-pnp-docs />
        </template>
      </q-input>
      <q-input
        :dark="isDarkModeGlobal"
        filled
        dense
        for="isppa"
        label="Isppa [W/cm2]"
        type="number"
        step="0.01"
        min="0.001"
        v-model="isppa"
        :rules="rules"

        @update:modelValue="executeCalculation(isppa, 'isppa')"
        hide-bottom-space
      >
        <template #append>
          <reference-popup-proxy
            :icon="'help'"
            tooltip="Show note"
            :text="isppaMessage"
          />
        </template>
      </q-input>
      <q-input
        :dark="isDarkModeGlobal"
        filled
        dense
        for="MI"
        label="MI (focal depth dependent)"
        type="number"
        step="0.01"
        v-model="MI"
        :rules="rules"
        hide-bottom-space

        @update:modelValue="executeCalculation(MI, 'MI')"
      >
        <template #append>
          <reference-popup-proxy
            :icon="'help'"
            tooltip="Show note"
            :text="`Mechanical index, standardised indicator related to the potential for mechanical bioeffects, specifically cavitation. The peak-rarefactional pressure derated using an attenuation coefficient of 0.3 dB/cm/MHz is used in this expression, as this is the formal definition of MI. This deration considers the pathway from skin to focus, assuming minimal loss through the coupling medium. Please see the ITRUSST Standardized Reporting Consensus for further explanation.`"
          />
        </template>
      </q-input>
      <q-input
        :dark="isDarkModeGlobal"
        filled
        dense
        for="frequency_MHz"
        min="0.001"
        label="Fundamental Frequency [MHz]"
        type="number"
        step="0.001"
        v-model="frequency_MHz"
        :rules="rules"
        hide-bottom-space

        @update:modelValue="executeCalculation(frequency_MHz, 'pr')"
      />
    </q-card-section>
    <q-separator />
    <q-card-section
      class="q-gutter-xs q-pa-sm"
    >
      <q-toolbar>
        <q-toolbar-title class="text-h7">
          Timing
        </q-toolbar-title>

        <q-btn
          v-if="timingParameters"
          :dark="isDarkModeGlobal"
          fab-mini
          color="primary"
          icon="graphic_eq"
          size="xs"
          dense
          @click="calculatorStore.showTimingGraph = true"
          flat
        >
          Graph
          <q-tooltip>View Timing Graph</q-tooltip>
        </q-btn>

        <q-btn
          :dark="isDarkModeGlobal"
          color="teal-4"
          v-if="parametersChanged && time_parameters_valid"
          icon="calculate"
          label="Calculate"
          push
          @click="calculatorStore.recalculate()"
        />
        <timing-docs />
      </q-toolbar>
      <TimingParameters />
      <q-toolbar>
        <q-toolbar-title class="text-h7" />

        <q-btn
          :dark="isDarkModeGlobal"
          color="teal-4"
          v-if="parametersChanged && time_parameters_valid && calculatorStore.allTimeTogglesOn"
          icon="calculate"
          label="Calculate"
          push
          @click="calculatorStore.recalculate()"
        />
      </q-toolbar>
    </q-card-section>
    <q-separator />
    <q-card-section
      class="q-gutter-xs q-pa-sm"
    >
      <q-toolbar>
        <q-toolbar-title class="text-h7">
          Assumptions
        </q-toolbar-title>
        <q-circular-progress
          :min="0"
          :max="1"
          :value="loadingValue"
          size="20px"
          color="primary"
          class="q-ma-md"
        />
        <reference-popup-proxy
          icon="warning"
          color="red"
          tooltip="Disclaimer"
          size="md"
          text="The assumption is made that the pressure is sufficiently low for nonlinearity to be negligible. Please see ITRUSST Standardized Reporting paper for information about situations in which nonlinearity occurs."
        />
      </q-toolbar>
      <q-input
        :dark="isDarkModeGlobal"
        label="Density of brain [kg/m3]"
        filled
        dense
        type="number"
        step="0.1"
        v-model="rho_kg_m3"
        :rules="rules"
        hide-bottom-space

        @update:modelValue="executeCalculation(rho_kg_m3, 'pr')"
      >
        <template #append>
          <reference-popup-proxy
            :link="'https://doi.org/10.13099/VIP21000-04-1'"
          />
        </template>
      </q-input>
      <q-input
        :dark="isDarkModeGlobal"
        label="Speed Of Sound of Brain [m/s]"
        filled
        dense
        type="number"
        step="0.1"
        v-model="c_m_s"
        :rules="rules"

        @update:modelValue="executeCalculation(c_m_s, 'pr')"
        hide-bottom-space
      >
        <template #append>
          <reference-popup-proxy
            :link="'https://doi.org/10.13099/VIP21000-04-1'"
          />
        </template>
      </q-input>
      <q-input
        :dark="isDarkModeGlobal"
        :model-value="calculatorStore.z_Rayl"
        readonly
        outlined
        label="Acoustic Impedance [kg/(mm2*s)]"
        dense
        type="number"
      />
    </q-card-section>

    <q-card-section
      class="q-gutter-xs q-pa-sm"
    >
      <q-toolbar>
        <q-toolbar-title class="text-h7">
          Acoustic power
        </q-toolbar-title>
<q-circular-progress
          :min="0"
          :max="1"
          :value="loadingValue"
          size="20px"
          color="primary"
          class="q-ma-md"
        />

      </q-toolbar>

      <q-input
        :dark="isDarkModeGlobal"
        label="Total Electrical Power [mW]"
        filled
        dense
        type="number"
        step="1"
        v-model="electrical_power"
        :rules="rules"
        hide-bottom-space
        @update:modelValue="executeCalculation(electrical_power, 'electrical_power')"
      >
        <template #append>
          <reference-popup-proxy
            :icon="'help'"
            tooltip="Show note"
            :text="'Represents the power delivered to the ultrasound transducer to produce ultrasound waves, measured in milliwatts (mW).'"
          />
        </template>
      </q-input>

      <q-input
        :dark="isDarkModeGlobal"
        label="Transducer efficiency [%]"
        filled
        dense
        type="number"
        step="0.1"
        v-model="transducer_efficiency"
        :rules="rules"
        hide-bottom-space
        @update:modelValue="executeCalculation(transducer_efficiency, 'transducer_efficiency')"
      >
        <template #append>
          <reference-popup-proxy
            :icon="'help'"
            tooltip="Show note"
            :text="'Represents the ratio of acoustic power output to electrical power input, expressed as a percentage. It indicates how effectively the transducer converts electrical energy into ultrasound energy.'"
          />
        </template>
      </q-input>

      <q-input
        :dark="isDarkModeGlobal"
        label="Total Acoustic Power [mW]"
        filled
        dense
        type="number"
        step="1"
        v-model="acoustic_power"
        :rules="rules"
        hide-bottom-space
        @update:modelValue="executeCalculation(acoustic_power, 'acoustic_power')"
      >
        <template #append>
          <reference-popup-proxy
            :icon="'help'"
            tooltip="Show note"
            :text="'Represents the ultrasound energy output from the transducer, measured in milliwatts (mW). This is the effective power transmitted as sound waves. Note that in the TUS Calculator, the acoustic power is treated as independent of the pressure amplitude, which differs from real-world behavior. The calculator uses the acoustic power to specifically calculate the TIC in the skull, while the pressure amplitude is used for metrics such as the Isppa.'"
          />
        </template>
      </q-input>
    </q-card-section>
    <q-separator />

    <q-separator />
    <q-card-section
      v-show="derate"
      class="q-gutter-xs q-pa-sm"
    >
      <q-toolbar>
        <q-toolbar-title class="text-h7">
          Derating values
        </q-toolbar-title>
        <q-circular-progress
          :min="0"
          :max="1"
          :value="loadingValue"
          size="20px"
          color="primary"
          class="q-ma-md"
        />
        <thickness-docs />
      </q-toolbar>
      <q-input
        :dark="isDarkModeGlobal"
        v-show="deratingParameters"
        filled
        dense
        for="diameter_of_transducer"
        label="Diameter of transducer [mm]"
        type="number"
        step="1"
        min="0"
        v-model="diameter_of_transducer_mm"
        :rules="rules"
        @update:modelValue="executeCalculation(diameter_of_transducer_mm, 'pr')"
        hide-bottom-space
      >
        <template #append>
          <reference-popup-proxy
            :icon="'help'"
            tooltip="Show note"
            :text="`Diameter measured at transducer exit plane.`"
          />
        </template>
      </q-input>

      <q-input
        :dark="isDarkModeGlobal"
        v-show="deratingParameters"
        filled
        dense
        for="coupling_thickness_mm"
        label="Coupling thickness [mm]"
        type="number"
        step="1"
        v-model="coupling_thickness_mm"
        :rules="rules"

        @update:modelValue="executeCalculation(coupling_thickness_mm, 'pr')"
        hide-bottom-space
      >
        <template #append>
          <reference-popup-proxy
            :icon="'help'"
            tooltip="Show note"
            :text="` Distance from transducer exit plane to skin surface.`"
          />
        </template>
      </q-input>
      <q-input
        :dark="isDarkModeGlobal"
        v-show="deratingParameters"
        filled
        dense
        for="thickness_of_scalp"
        label="Scalp thickness [mm]"
        type="number"
        step="0.1"
        v-model="thickness_of_scalp_mm"
        :rules="rules"
        @update:modelValue="executeCalculation(thickness_of_scalp_mm, 'pr')"
        hide-bottom-space
      >
        <template #append>
          <reference-popup-proxy
            :link="'https://doi.org/10.1001/archderm.1954.01540230051006'"
          />

          <reference-popup-proxy
            :icon="'help'"
            tooltip="Show note"
            :text="`Distance from skin surface to outer surface of skull.`"
          />
        </template>
      </q-input>
      <q-input
        :dark="isDarkModeGlobal"
        v-show="deratingParameters"
        filled
        dense
        for="thickness_of_skull"
        label="Skull thickness [mm]"
        type="number"
        step="1"
        v-model="thickness_of_skull_mm"
        :rules="rules"

        @update:modelValue="executeCalculation(thickness_of_skull_mm, 'pr')"
        hide-bottom-space
      >
        <template #append>
          <reference-popup-proxy
            icon="warning"
            color="red"
            tooltip="Disclaimer"
            text="Default is worst case assumption of thickness of skull."
          />

          <reference-popup-proxy
            :icon="'help'"
            tooltip="Show note"
            :text="`Distance from outer to inner surface of skull.`"
          />
        </template>
      </q-input>
      <q-input
        :dark="isDarkModeGlobal"
        v-show="deratingParameters"
        filled
        dense
        for="focal_depth"
        label="Focal depth [mm]"
        type="number"
        step="1"
        v-model="focal_depth_mm"
        :rules="[
          val => !!val || 'This field is required',
          val => val > 0 || 'Value must be > 0',
          val => val >= minimumValueFocalDepth || `Value must be >= ${minimumValueFocalDepth} (coupling thickness + scalp thickness + skull thickness)`
        ]"

        @update:modelValue="executeCalculation(focal_depth_mm, 'pr')"
        hide-bottom-space
      >
        <template #append>
          <reference-popup-proxy
            :icon="'help'"
            tooltip="Show note"
            :text="`Measured from transducer exit plane.`"
          />
        </template>
      </q-input>

      <q-input
        :dark="isDarkModeGlobal"
        v-show="deratingParameters"
        outlined
        dense
        for="us_beam_diameter_on_skin_mm"
        label="US beam diameter on skin [mm]"
        type="number"
        step="1"
        readonly
        :model-value="calculatorStore.us_beam_diameter_on_skin_mm"
      >
        <template #append>
          <reference-popup-proxy
            :icon="'help'"
            tooltip="Show note"
            :text="`Diameter of ultrasound beam at the skin surface.`"
          />
        </template>
      </q-input>

      <q-input
        :dark="isDarkModeGlobal"
        v-show="deratingParameters"
        outlined
        dense
        for="us_beam_diameter_on_skull_mm"
        label="US beam diameter on skull [mm]"
        type="number"
        step="1"
        readonly
        :model-value="calculatorStore.us_beam_diameter_on_skull_mm"
      >
        <template #append>
          <reference-popup-proxy
            :icon="'help'"
            tooltip="Show note"
            :text="`Diameter of ultrasound beam at the skull surface.`"
          />
        </template>
      </q-input>

      <q-card
        :dark="isDarkModeGlobal"
        v-show="deratingParameters"
      >
        <q-toolbar>
          <q-toolbar-title>Skull</q-toolbar-title>
        </q-toolbar>
        <q-card-section class="q-gutter-xs q-pa-sm">
          <q-input
            :dark="isDarkModeGlobal"
            v-show="!isManualSkullPressureTransmission"
            outlined
            dense
            readonly
            for="Skull_pressure_transmission"
            label="Pressure transmission [-]"
            type="number"
            v-model="skull_pressure_transmission"
          >
            <template #append>
              <q-icon
                name="edit"
                @click="calculatorStore.switchToManualSkullPressureTransmission(true)"
              >
                <q-tooltip>
                  Set your own value for skull pressure transmission
                </q-tooltip>
              </q-icon>
              <reference-popup-proxy
                :icon="'help'"
                :link-text="'Attali et al. 2023.'"
                :link="'https://doi.org/10.1016/j.brs.2022.12.005'"
                :text="`Proportion of pressure transmitted in the skull expressed as a number from 0 to 1, with 0 representing complete attenuation and 1 representing no attenuation. Estimated initially based on entered fundamental frequency, ultrasound beam diameter and therefrom derived worst case estimate found in lookup table of`"
                :append_text="` When thickness of skull is known, performing a simulation is appropriate. Value can be adjusted accordingly. When modified, intensity loss calculation based on US beam diameter and fundamental frequency of look-up table is overruled. Use the reset button to undo the overruling.`"
              />
            </template>
          </q-input>

          <q-input
            :dark="isDarkModeGlobal"
            v-show="isManualSkullPressureTransmission"
            dense
            filled
            for="Skull_pressure_transmission"
            label="Pressure transmission [-]"
            type="number"
            v-model="skull_pressure_transmission_manual"
            step="0.01"

            :rules="[val => !!val || 'This field is required', val => val > 0 || 'Value must be > 0']"
            @update:modelValue="executeCalculation(skull_pressure_transmission_manual, 'pr')"
            hide-bottom-space
          >
            <template #append>
              <reference-popup-proxy
                :link="'https://doi.org/10.1016/j.brs.2022.12.005'"
                :text="`Reference: David Attali, Thomas Tiennot, Mark Schafer, Elsa Fouragnan, Jérôme Sallet, Charles F Caskey, Robert Chen, Ghazaleh Darmani, Ellen J. Bubrick, Christopher Butler, Charlotte J Stagg, Miriam Klein-Flügge, Lennart Verhagen, Seung-Schik Yoo, Kim Butts Pauly, Jean-Francois Aubry, Three-layer model with absorption for conservative estimation of the maximum acoustic transmission coefficient through the human skull for transcranial ultrasound stimulation, Brain Stimulation, Volume 16, Issue 1, 2023, Pages 48-55, ISSN 1935-861X, `"
              />

              <reference-popup-proxy
                :icon="'help'"
                tooltip="Show note"
                :text="`
                When modified, intensity loss calculation based on US beam diameter and fundamental frequency of look-up table is overruled. Use the reset button to undo the overruling.`"
              />
              <q-icon
                name="refresh"
                @click="calculatorStore.switchToManualSkullPressureTransmission(false)"
              >
                <q-tooltip>
                  Use intensity loss calculated value
                </q-tooltip>
              </q-icon>
            </template>
          </q-input>
        </q-card-section>
      </q-card>

      <q-card
        :dark="isDarkModeGlobal"
        v-show="deratingParameters"
      >
        <q-toolbar>
          <q-toolbar-title>Brain</q-toolbar-title>
        </q-toolbar>
        <q-card-section class="q-gutter-xs q-pa-sm">
          <q-input
            :dark="isDarkModeGlobal"
            v-show="deratingParameters"
            outlined
            dense
            readonly
            label="Pressure transmission [-]"
            type="number"
            v-model="brain_pressure_transmission"
          >
            <template #append>
              <reference-popup-proxy
                :icon="'help'"
                tooltip="Show note"
                :text="`Proportion of pressure transmitted in the brain expressed as a number from 0 to 1, with 0 representing complete attenuation and 1 representing no attenuation. Estimated based on entered fundamental frequency, attenuation coefficient and distance that the beam travels in the brain derived from entered coupling thickness, scalp thickness, skull thickness and focal depth.`"
              />
            </template>
          </q-input>

          <q-input
            :dark="isDarkModeGlobal"
            v-show="deratingParameters"
            outlined
            dense
            readonly
            label="Attenuation coefficient [dB/cm/MHz]"
            type="number"
            v-model="coeff_dB_cm_MHz"
          >
            <template #append>
              <reference-popup-proxy

                :link="'https://doi.org/10.13099/VIP21000-04-1'"
              />
              <reference-popup-proxy
                :icon="'help'"
                tooltip="Show note"
                :text="attenuationCoeffMessage"
              />
            </template>
          </q-input>
          <q-input
            :dark="isDarkModeGlobal"
            v-show="deratingParameters"
            outlined
            dense
            readonly
            label="Absorption attenuation coefficient [db/cm/MHz]"
            type="number"
            v-model="att_coef_abs"
          >
            <template #append>
              <temp-rise-equation-dialog />
            </template>
          </q-input>
        </q-card-section>
      </q-card>
    </q-card-section>
  </q-card>
</template>