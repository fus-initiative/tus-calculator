<template>
  <q-toolbar>
    <q-toolbar-title>
      Regulatory limits on pressure
    </q-toolbar-title>

    <q-btn
      color="primary"
      icon="download"
      flat
      dense
      @click="saveChartAsImage"
    >
      <q-tooltip>
        Save Chart as Image
      </q-tooltip>
    </q-btn>

    <q-btn
      icon="help"
      color="primary"
      dense
      flat
    >
      <q-tooltip>
        Show note
      </q-tooltip>
      <q-popup-proxy :offset="[-70, 20]">
        <q-banner :dark="isDarkModeGlobal">
          <template #avatar>
            <q-icon
              name="help"
              color="primary"
            />
          </template>
          The mechanical index (MI) is estimated using the peak-rarefactional pressure in MPa derated using an attenuation coefficient of 0.3 dB/cm/MHz (see ITRUSST consensus on standardised reporting for details). Since the derated pressure changes with depth, MI also changes with depth. This graph shows the MI limit at a depth equal to the focal depth minus the coupling distance.
        </q-banner>
      </q-popup-proxy>
    </q-btn>
  </q-toolbar>
  <v-chart
    ref="chartRef"
    id="chartRef"
    class="chart"
    :option="chartOptions"
    :init-options="initOptions"
    autoresize
  />
</template>

<script setup lang="ts">
import { storeToRefs } from "pinia"
import { useCalculatorStore } from '../stores/calculator.store'
const calculatorStore = useCalculatorStore()
import { EChartsOption } from 'echarts';

const {
  ppp_mpa,
  derated_ppp_mpa,
  frequency_MHz,
  isDarkModeGlobal,
  coupling_thickness_mm,
  focal_depth_mm
} = storeToRefs(calculatorStore)

import * as echarts from 'echarts/core'
import { SVGRenderer } from 'echarts/renderers'
import {
  TitleComponent,
  ToolboxComponent,
  TooltipComponent,
  GridComponent,
  LegendComponent,
  GraphicComponent
} from 'echarts/components'
import { LineChart } from 'echarts/charts'
import { UniversalTransition } from 'echarts/features'

import VChart from 'vue-echarts'
import { reactive, ref, watch} from 'vue'
import type ECharts from "vue-echarts";

echarts.use([
  TitleComponent,
  ToolboxComponent,
  TooltipComponent,
  GridComponent,
  LegendComponent,
  LineChart,
  SVGRenderer,
  UniversalTransition,
  GraphicComponent
])

// Reference to the chart component
const chartRef = ref<InstanceType<typeof ECharts> | null>(null)

function writeToFile(svgString) {
  const blob = new Blob([svgString], { type: 'image/svg+xml' })
  const url = URL.createObjectURL(blob)
  const a = document.createElement('a')
  a.href = url;
  a.download = `plot.svg`
  document.body.appendChild(a);
  a.click();
  document.body.removeChild(a);
  URL.revokeObjectURL(url);
}

function saveChartAsImage () {
  if (chartRef.value) {
    writeToFile(chartRef.value.chart.renderToSVGString())
  }
}

const initOptions = {
        renderer: "svg",
        devicePixelRatio: 2,
}

// https://echarts.apache.org/en/option.html
const chartOptions = reactive<EChartsOption>({
  darkMode: 'auto',
  color: ['#5470C6', '#66EEE9FF', '#FF2424FF', '#b500f1'],
  title: {
    text: 'Regulatory limits on pressure',
    left: 'center',

     textStyle: {
            color: isDarkModeGlobal.value ? "#FFF" : "#333",

      fontSize: 18, // You can adjust the font size here
      fontFamily: '"Roboto", "-apple-system", "Helvetica Neue", Helvetica, Arial, sans-serif', // Specify the font family here
      fontWeight: 'normal',
    },
  },
  tooltip: {
    trigger: 'axis'
  },
  legend: {
    top: 30,
    left: 'center',
    backgroundColor: isDarkModeGlobal.value ? "#9a9595" : "#FFF",
    data: ['Not Derated Pr','Derated Pr', 'Pr (MI limit)', 'Pr (ISPPA limit)']
  },
  xAxis: {
    name: 'Frequency (MHz)',
    nameLocation: 'middle',
    nameTextStyle: {
      color: isDarkModeGlobal.value ? "#FFF" : "#333",
      padding: 4
    },
    minorTick: {
      show: true
    },
    minorSplitLine: {
      show: false
    }
  },
  yAxis: {
    name: 'Pressure (MPa)',
    nameLocation: 'center',
    nameTextStyle: {
      color: isDarkModeGlobal.value ? "#FFF" : "#333",
      padding: 4
    },
    min: 0,
    max: 4,
    minorTick: {
      show: true
    },
    minorSplitLine: {
      show: false
    }
  },
  series: [
    {
      name: 'Pr (MI limit)',
      type: 'line',
      showSymbol: frequency_MHz.value <= 3,
      clip: false,
      smooth: false,
      data: calculatorStore.generatePrMIData()
    },
    {
      name: 'Pr (ISPPA limit)',
      type: 'line',
      showSymbol: true,
      clip: false,
      smooth: false,
      data: calculatorStore.generatePrISPPAData()
    },
    {
      name: 'Not Derated Pr',
      type: 'line',
      showSymbol: true,
      symbolSize: 10,
      data: calculatorStore.getMyNotDeratedValue()
    },
    {
      name: 'Derated Pr',
      type: 'line',
      showSymbol: true,
      symbolSize: 10,
      data: calculatorStore.getMyValue()
    }
  ],
  graphic: [{
    type: "text",
    left: "11%",
    top: "15%",
    style: {
      text: `Focal depth - coupling thickness = ${calculatorStore.depth_mm} mm` ,
      font: "bold 16px sans-serif",
      fill: isDarkModeGlobal.value ? "#FFF" : "#333",
      textAlign: "center",
      textVerticalAlign: "middle",
    },
  },
  {
    type: "text",
    left: "11%",
    top: "20%",
    style: {
      text: `MI limit for a depth of ${calculatorStore.depth_mm} mm` ,
      font: "bold 16px sans-serif",
      fill: isDarkModeGlobal.value ? "#FFF" : "#333",
      textAlign: "center",
      textVerticalAlign: "middle",
    },
  }]
})

watch(isDarkModeGlobal, () => {
  chartOptions.darkMode = isDarkModeGlobal.value
})

watch([ppp_mpa, coupling_thickness_mm, focal_depth_mm] , () => {
  chartOptions.graphic = [{
    type: "text",
    left: "11%",
    top: "15%",
    style: {
      text: `Focal depth - coupling thickness = ${calculatorStore.depth_mm} mm` ,
      font: "bold 16px sans-serif",
      fill: isDarkModeGlobal.value ? "#FFF" : "#333",
      textAlign: "center",
      textVerticalAlign: "middle",
    },
  },
  {
    type: "text",
    left: "11%",
    top: "20%",
    style: {
      text: `MI limit for a depth of ${calculatorStore.depth_mm} mm` ,
      font: "bold 16px sans-serif",
      fill: isDarkModeGlobal.value ? "#FFF" : "#333",
      textAlign: "center",
      textVerticalAlign: "middle",
    },
  }]
  chartOptions.series = [
      {
        name: 'Pr (MI limit)',
        type: 'line',
        showSymbol: true,
        clip: false,
        smooth: false,
        data: calculatorStore.generatePrMIData()
      },
    {
      name: 'Pr (ISPPA limit)',
      type: 'line',
      showSymbol: true,
      clip: false,
      smooth: false,
      data: calculatorStore.generatePrISPPAData()
    },
    {
      name: 'Not Derated Pr',
      type: 'line',
      showSymbol: true,
      symbolSize: 10,
      data: calculatorStore.getMyNotDeratedValue()
    },
    {
      name: 'Derated Pr',
      type: 'line',
      showSymbol: true,
      symbolSize: 10,
      data: calculatorStore.getMyValue()
    }
  ]
})

watch(derated_ppp_mpa, () => {
  chartOptions.graphic = [{
    type: "text",
    left: "11%",
    top: "15%",
    style: {
      text: `Focal depth - coupling thickness = ${calculatorStore.depth_mm} mm` ,
      font: "bold 16px sans-serif",
      fill: isDarkModeGlobal.value ? "#FFF" : "#333",
      textAlign: "center",
      textVerticalAlign: "middle",
    },
  },
  {
    type: "text",
    left: "11%",
    top: "20%",
    style: {
      text: `MI limit for a depth of ${calculatorStore.depth_mm} mm` ,
      font: "bold 16px sans-serif",
      fill: isDarkModeGlobal.value ? "#FFF" : "#333",
      textAlign: "center",
      textVerticalAlign: "middle",
    },
  }]

  chartOptions.series = [
      {
        name: 'Pr (MI limit)',
        type: 'line',
        showSymbol: true,
        clip: false,
        smooth: false,
        data: calculatorStore.generatePrMIData()
      },
    {
      name: 'Pr (ISPPA limit)',
      type: 'line',
      showSymbol: true,
      clip: false,
      smooth: false,
      data: calculatorStore.generatePrISPPAData()
    },
    {
      name: 'Not Derated Pr',
      type: 'line',
      showSymbol: true,
      symbolSize: 10,
      data: calculatorStore.getMyNotDeratedValue()
    },
    {
      name: 'Derated Pr',
      type: 'line',
      showSymbol: true,
      symbolSize: 10,
      data: calculatorStore.getMyValue()
    }
  ]
})



</script>

<style scoped>
.chart {
  max-height: 500px;
  max-width: 800px;
}
</style>