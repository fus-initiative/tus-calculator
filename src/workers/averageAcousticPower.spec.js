// worker.test.ts
import '@vitest/web-worker'
import {describe, expect, it} from "vitest"

const message = {
    "acoustic_power": 3750,
    "nPulses": 1,
    "pulseDur": 5,
    "pulseRepetitionInterval": 5,
    "pulseTrainDuration": 5,
    "pulseTrainRampDuration": 0,
    "pulseTrainRampShape": "Tukey",
    "pulseTrainRepetitionInterval": 5,
    "pulseTrainRepetitionDuration": 5,
    "pulseTrainRepetitionRampShape": "Tukey",
    "pulseTrainRepetitionRampDuration": 0,
    "nTrains": 1,
    "pulse_ramp_shape": "Tukey",
    "pulse_ramp_duration_ms": 0,
    "pulseTrainRepetitionIntervalWidth": 1000,
    "pulseRepetitionIntervalWidth": 1000,
    "pulseOnly": true,
    "pulseAndTrainOnly": false,
    "allTimeTogglesOn": false,
    "frequency_MHz": 0.5,
    "ppp_mpa": "0.79",
    "derated_ppp_mpa": 0.44961270000000003,
    "z_Rayl": 1.62,
    "linkColor": "rgb(238,227,227, 50)",
    "canvas": null,
    "padding": 5,
    "arbitrary_default_amplitude": 100,
    "amplitude": 79,
    "canvas_width": 1000,
    "canvas_height": 1000,
    "showPulseTrain": false,
    "showPulseTrainRepetition": false,
    "ctx": null,
    "axisColor": "black",
    "textColor": "black",
    "pulseFillStyle": "red",
    "backgroundColor": "white",
    "canvasBlock": 100,
    "isppaOld": 19.26234567918141,
    "n": 10000000,
    "n_rep": 50000000,
    "isppa": 19.26,
    "derated_isppa": 6.04,
    "derated_isppaOld": 6.239246296357843,
    "pampl": 790000,
    "derated_pampl": 449612.7,
    "Z": 1620000
}

function getTIC(avg_acoustic_power, us_beam_diameter_on_skull_cm) {
    return +(+avg_acoustic_power / us_beam_diameter_on_skull_cm / 40).toFixed(2)
}

function getResult(worker) {
    return new Promise((resolve, reject) => {
        worker.onmessage = function (e) {
            resolve(e.data);
        };
        worker.onerror = function (err) {
            reject(err);
        };
    });
}

describe('Web Worker', () => {
    it('calculates the average acoustical power values upon change pulseDur', async () => {
        const worker = new Worker(new URL('./averageAcousticPower.js', import.meta.url), {type: 'module'});
        const us_beam_diameter_on_skull_cm = 51.2
        const expectedAverageAcousticPower = 1875
        const expectedTIC = 0.92
        const pulseDurChangeMessage = Object.assign({}, message)
        const result = getResult(worker)
        worker.postMessage(pulseDurChangeMessage)

        try {
            const data = await result
            expect(data.job).toBe('average_acoustic_power')
            expect(data.average_acoustic_power).toBe( expectedAverageAcousticPower)
            expect(getTIC(data.average_acoustic_power, us_beam_diameter_on_skull_cm)).toBe(expectedTIC)
        } finally {
            // Terminate the worker after the test
            worker.terminate()
        }
    });

    it('calculates the average acoustical power values upon change pulseDur to 10ms', async () => {
            const worker = new Worker(new URL('./averageAcousticPower.js', import.meta.url), {type: 'module'});
            const us_beam_diameter_on_skull_cm = 51.2
            const expectedAverageAcousticPower = 1875
            const expectedTIC = 0.92

            const pulseDurChangeMessage = Object.assign({}, message)
            pulseDurChangeMessage.pulseDur = 10
            const result = getResult(worker)

            worker.postMessage(pulseDurChangeMessage);

            try {
                const data = await result;
                expect(data.job).toBe('average_acoustic_power');
                expect(data.average_acoustic_power).toBe( expectedAverageAcousticPower);
                expect(getTIC(data.average_acoustic_power, us_beam_diameter_on_skull_cm)).toBe(expectedTIC);
            } finally {
                worker.terminate();
            }
        });

    it('pulse duration 5 ms or something else, total acoustic power is 3750 mW, diameter on skull is 4.38 cm ->' +
        ' Results in a TIC of 10.7 mW/cm.', async () => {
            const worker = new Worker(new URL('./averageAcousticPower.js', import.meta.url), {type: 'module'});
            const us_beam_diameter_on_skull_cm = 4.38
            const expectedAverageAcousticPower = 1875
            const expectedTIC = 10.7

            const pulseDurChangeMessage = Object.assign({}, message)
            const result = getResult(worker)

            // Send a message to the worker
            worker.postMessage(pulseDurChangeMessage)
            try {
                const data = await result;
                expect(data.job).toBe('average_acoustic_power')
                expect(data.average_acoustic_power).toBe( expectedAverageAcousticPower)
                expect(getTIC(data.average_acoustic_power, us_beam_diameter_on_skull_cm)).toBe(expectedTIC)
            } finally {
                worker.terminate()
            }
        });


    it('Test case pulse train: Pulse duration of 100 ms, pulse repetition interval of 200 ms,' +
        ' total acoustic power is 3750 mW, diameter on skull is 4.38 cm -> Results in a TIC of 5.35 mW/cm.', async () => {
        const worker = new Worker(new URL('./averageAcousticPower.js', import.meta.url), {type: 'module'});
        const us_beam_diameter_on_skull_cm = 4.38
        const expectedAverageAcousticPower = 938
        const expectedTIC = 5.35

        const m = Object.assign({}, message)
        m.pulseDur = 100
        m.pulseRepetitionInterval = 200
        m.pulseTrainDuration = 200
        m.pulseTrainRepetitionInterval = 200
        m.acoustic_power = 3750
        m.pulseAndTrainOnly = true
        const result = getResult(worker)
        // Send a message to the worker
        worker.postMessage(m)

        try {
            const data = await result
            expect(data.job).toBe('average_acoustic_power')
            expect(+data.average_acoustic_power.toFixed(0)).toBe( expectedAverageAcousticPower)
            expect(getTIC(data.average_acoustic_power, us_beam_diameter_on_skull_cm)).toBe( expectedTIC)
        } finally {
            // Terminate the worker after the test
            worker.terminate()
        }
    });

    it('Test case pulse train repetition: Pulse duration of 100 ms, pulse repetition interval of 200 ms, PTR interval of 400ms,' +
        ' total acoustic power is 3750 mW, diameter on skull is 4.38 cm -> Results in a TIC of 2.69 mW/cm.', async () => {
        const worker = new Worker(new URL('./averageAcousticPower.js', import.meta.url), {type: 'module'});
        const us_beam_diameter_on_skull_cm = 4.38
        const expectedAverageAcousticPower = 469
        const expectedTIC = 2.68

        const m = Object.assign({}, message);
        m.pulseDur = 100
        m.pulseRepetitionInterval = 200
        m.pulseTrainDuration = 200
        m.pulseTrainRepetitionInterval = 400
        m.pulseTrainRepetitionDuration = 400
        m.acoustic_power = 3750
        m.allTimeTogglesOn = true

        const result= getResult(worker)

        // Send a message to the worker
        worker.postMessage(m);

        try {
            const data = await result;
            expect(data.job).toBe('average_acoustic_power');
            expect(+data.average_acoustic_power.toFixed(0)).toBe( expectedAverageAcousticPower);
            expect(getTIC(data.average_acoustic_power, us_beam_diameter_on_skull_cm)).toBe( expectedTIC);
        } finally {
            // Terminate the worker after the test
            worker.terminate();
        }
    });
})
