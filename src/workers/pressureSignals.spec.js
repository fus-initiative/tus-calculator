// worker.test.ts
import '@vitest/web-worker'
import {describe, expect, it} from "vitest"

const message = {
    pulseOnly: true,
    pulseAndTrainOnly: true,
    allTimeTogglesOn: true,
    job: 'isppa',
    z_Rayl: 1.62,
    pulseDur: 5,
    rampDur: 0,
    pulseRepetitionInterval: 10,
    pulseTrainDuration: 10,
    pulseTrainRampDuration: 0,
    pulseTrainRepetitionInterval: 10,
    nPulses: 1,
    nTrains: 1,
    time_parameters_valid: true,
    pulse_ramp_shape: "Tukey",
    pulse_ramp_duration_ms: 0,
    showPulseTrain: true,
    showPulseTrainRepetition: true,
    ppp_mpa: 0.79,
    derated_ppp_mpa: 0.44,
    frequency_MHz: 0.5,
    pulseTrainRepetitionDuration: 10,
    pulseTrainRampShape: "Tukey",
    pulseTrainRepetitionRampShape: "Tukey",
    pulseTrainRepetitionRampDuration: 0,
}

function getResult(worker) {
    return new Promise((resolve, reject) => {
        worker.onmessage = function (e) {
            resolve(e.data);
        };
        worker.onerror = function (err) {
            reject(err);
        };
    });
}

describe('Web Worker Test Pressure Signals', () => {

    it('calculates the ISPTA values upon change pulseDur', async () => {
        const worker = new Worker(new URL('./pressureSignals.js', import.meta.url), {type: 'module'});

        const pulseDurChangeMessage = Object.assign({}, message);
        pulseDurChangeMessage.pulseDur = 7.5
        const result = getResult(worker)

        // Send a message to the worker
        worker.postMessage(pulseDurChangeMessage);

        try {
            const data = await result;
            expect(data.job).toBe('isppa');
            expect(data.isppa).toBe(19.26);
            expect(data.derated_isppa).toBe(5.98);
            expect(data.ispta_pulse_train).toBe(14.45);
            expect(data.derated_ispta_pulse_train).toBe(4.48);
            expect(data.ispta_pulse_train_repetition).toBe(14.45);
            expect(data.derated_ispta_pulse_train_repetition).toBe(4.48);
        } finally {
            // Terminate the worker after the test
            worker.terminate();
        }
    });

    it('calculates the ISPTA values upon change pressure MPa', async () => {
      const worker = new Worker(new URL('./pressureSignals.js', import.meta.url), { type: 'module' });

      const pulseDurChangeMessage = Object.assign({}, message);
      pulseDurChangeMessage.ppp_mpa = 1.0
      pulseDurChangeMessage.derated_ppp_mpa = 0.56

      const result = getResult(worker)
      // Send a message to the worker
      worker.postMessage(pulseDurChangeMessage);

      try {
          const data = await result;
          expect(data.job).toBe('isppa');
          expect(data.isppa).toBe(30.86);
          expect(data.derated_isppa).toBe(9.68);
          expect(data.ispta_pulse_train).toBe(15.43);
          expect(data.derated_ispta_pulse_train).toBe(4.84);
          expect(data.ispta_pulse_train_repetition).toBe(15.43);
          expect(data.derated_ispta_pulse_train_repetition).toBe(4.84);
      } finally {
          // Terminate the worker after the test
          worker.terminate();
      }
    });

    it('calculates the Pampl values upon change ISPPA', async () => {
      const worker = new Worker(new URL('./pressureSignals.js', import.meta.url), { type: 'module' });

      const pamplJobMessage = Object.assign({}, message);
      pamplJobMessage.job = 'pampl'
      pamplJobMessage.isppa = 25
      pamplJobMessage.d_reas_coeff = 0.6
      pamplJobMessage.brain_pressure_transmission = 0.933

      const result = getResult(worker)
      worker.postMessage(pamplJobMessage);
      try {
          const data = await result;
          expect(data.job).toBe('pampl-result');
          expect(+(data.ppp_mpa.toFixed(2))).toBe(0.90);
          expect(+(data.derated_ppp_mpa.toFixed(2))).toBe(0.50);
      } finally {
          // Terminate the worker after the test
          worker.terminate();
      }
    });

    it('calculates the Pampl values upon pulse and pulse train repe', async () => {
      const worker = new Worker(new URL('./pressureSignals.js', import.meta.url), { type: 'module' });

      const pamplJobMessage = Object.assign({}, message);
      pamplJobMessage.pulseDur = 20
      pamplJobMessage.pulseTrainRepetitionInterval = 40

      const pulseMessage = {
        pulseOnly: true,
        pulseAndTrainOnly: true,
        allTimeTogglesOn: true,
        "job": "isppa",
        "isppa": 19.26,
        "derated_isppa": 5.98,
        "z_Rayl": 1.62,
        "pulseDur": 5,
        "rampDur": 0,
        "pulseRepetitionInterval": 10,
        "pulseTrainDuration": 20,
        "pulseTrainRampDuration": 0,
        "pulseTrainRepetitionInterval": 40,
        "nPulses": 2,
        "nTrains": 1,
        "time_parameters_valid": true,
        "pulse_ramp_shape": "Tukey",
        "pulse_ramp_duration_ms": 0,
        "showPulseTrain": true,
        "showPulseTrainRepetition": false,
        "ppp_mpa": 0.79,
        "derated_ppp_mpa": 0.44,
        "frequency_MHz": 0.5,
        "pulseTrainRepetitionDuration": 40,
        "pulseTrainRampShape": "Tukey",
        "pulseTrainRepetitionRampShape": "Tukey",
        "pulseTrainRepetitionRampDuration": 0
    }

    const result = getResult(worker)

    // Send a message to the worker
    worker.postMessage(pulseMessage);

    try {
        const data = await result;
        expect(data.job).toBe('isppa');
        expect(data.isppa).toBe(19.26);
        expect(data.ispta_pulse_train).toBe(9.63);
        expect(data.ispta_pulse_train_repetition).toBe(4.82);
        expect(data.derated_isppa).toBe(5.98);
        expect(data.derated_ispta_pulse_train).toBe(2.99);
        expect(data.derated_ispta_pulse_train_repetition).toBe(1.49);
    } finally {
        // Terminate the worker after the test
        worker.terminate();
    }
    });

    it('calculates the Pampl values upon pulse, pulse train and repetition', async () => {
      const worker = new Worker(new URL('./pressureSignals.js', import.meta.url), { type: 'module' });

      const pamplJobMessage = Object.assign({}, message)
      pamplJobMessage.pulseDur = 20
      pamplJobMessage.pulseTrainRepetitionInterval = 40

      const pulseMessage = {
        pulseOnly: true,
        pulseAndTrainOnly: true,
        allTimeTogglesOn: true,
        "job": "isppa",
        "isppa": 19.26,
        "derated_isppa": 5.98,
        "z_Rayl": 1.62,
        "pulseDur": 5,
        "rampDur": 0,
        "pulseRepetitionInterval": 10,
        "pulseTrainDuration": 20,
        "pulseTrainRampDuration": 0,
        "pulseTrainRepetitionInterval": 40,
        "nPulses": 2,
        "nTrains": 1,
        "time_parameters_valid": true,
        "pulse_ramp_shape": "Tukey",
        "pulse_ramp_duration_ms": 0,
        "showPulseTrain": true,
        "showPulseTrainRepetition": false,
        "ppp_mpa": 0.79,
        "derated_ppp_mpa": 0.44,
        "frequency_MHz": 0.5,
        "pulseTrainRepetitionDuration": 80,
        "pulseTrainRampShape": "Tukey",
        "pulseTrainRepetitionRampShape": "Tukey",
        "pulseTrainRepetitionRampDuration": 0
    }

    const result = getResult(worker)

      // Send a message to the worker
      worker.postMessage(pulseMessage)

      try {
          const data = await result
          expect(data.job).toBe('isppa')
          expect(data.isppa).toBe(19.26)
          expect(data.ispta_pulse_train).toBe(9.63)
          expect(data.ispta_pulse_train_repetition).toBe(4.82)

          expect(data.derated_isppa).toBe(5.98)
          expect(data.derated_ispta_pulse_train).toBe(2.99)
          expect(data.derated_ispta_pulse_train_repetition).toBe(1.49)
      } finally {
          // Terminate the worker after the test
          worker.terminate()
      }
    })


    it('calculates the ISPTRA values upon with a pulse train repetition duration of 200ms', async () => {
      const worker = new Worker(new URL('./pressureSignals.js', import.meta.url), { type: 'module' })
      const pulseMessage = {
        pulseOnly: true,
        pulseAndTrainOnly: true,
        allTimeTogglesOn: true,
        "job": "isppa",
        "isppa": 19.26,
        "derated_isppa": 5.98,
        "z_Rayl": 1.62,
        "pulseDur": 5,
        "rampDur": 0,
        "pulseRepetitionInterval": 10,
        "pulseTrainDuration": 20,
        "pulseTrainRampDuration": 0,
        "pulseTrainRepetitionInterval": 40,
        "nPulses": 2,
        "nTrains": 4,
        "time_parameters_valid": true,
        "pulse_ramp_shape": "Tukey",
        "pulse_ramp_duration_ms": 0,
        "showPulseTrain": true,
        "showPulseTrainRepetition": true,
        "ppp_mpa": 0.79,
        "derated_ppp_mpa": 0.442242,
        "frequency_MHz": 0.5,
        "pulseTrainRepetitionDuration": 200,
        "pulseTrainRampShape": "Tukey",
        "pulseTrainRepetitionRampShape": "Tukey",
        "pulseTrainRepetitionRampDuration": 0
    }

    const result = getResult(worker)

      // Send a message to the worker
      worker.postMessage(pulseMessage)

      try {
          const data = await result
          expect(data.job).toBe('isppa')
          expect(data.isppa).toBe(19.26)
          expect(data.ispta_pulse_train).toBe(9.63)
          expect(data.ispta_pulse_train_repetition).toBe(4.33)

          expect(data.derated_isppa).toBe(6.04)
          expect(data.derated_ispta_pulse_train).toBe(3.02)
          expect(data.derated_ispta_pulse_train_repetition).toBe(1.36)
      } finally {
          // Terminate the worker after the test
          worker.terminate()
      }
    })

it('calculates the ISPTRA values upon with a pulse train repetition duration of 240ms', async () => {
      const worker = new Worker(new URL('./pressureSignals.js', import.meta.url), { type: 'module' })
      const pulseMessage = {
        pulseOnly: true,
        pulseAndTrainOnly: true,
        allTimeTogglesOn: true,
        job: "isppa",
        isppa: 19.26,
        derated_isppa: 5.98,
        z_Rayl: 1.62,
        pulseDur: 5,
        rampDur: 0,
        pulseRepetitionInterval: 10,
        pulseTrainDuration: 20,
        pulseTrainRampDuration: 0,
        pulseTrainRepetitionInterval: 40,
        nPulses: 2,
        nTrains: 4,
        time_parameters_valid: true,
        pulse_ramp_shape: "Tukey",
        pulse_ramp_duration_ms: 0,
        showPulseTrain: true,
        showPulseTrainRepetition: true,
        ppp_mpa: 0.79,
        derated_ppp_mpa: 0.442242,
        frequency_MHz: 0.5,
        pulseTrainRepetitionDuration: 240,
        pulseTrainRampShape: "Tukey",
        pulseTrainRepetitionRampShape: "Tukey",
        pulseTrainRepetitionRampDuration: 0
    }

    const result = getResult(worker)

      // Send a message to the worker
      worker.postMessage(pulseMessage)

      try {
          const data = await result
          expect(data.job).toBe('isppa')
          expect(data.isppa).toBe(19.26)
          expect(data.ispta_pulse_train).toBe(9.63)
          expect(data.ispta_pulse_train_repetition).toBe(4.41)

          expect(data.derated_isppa).toBe(6.04)
          expect(data.derated_ispta_pulse_train).toBe(3.02)
          expect(data.derated_ispta_pulse_train_repetition).toBe(1.38)
      } finally {
          // Terminate the worker after the test
          worker.terminate()
      }
    })

})

