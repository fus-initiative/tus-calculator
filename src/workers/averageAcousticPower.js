import {
    trapezoidalRule,
    RAMPSHAPES,
    WorkerState,
    linearRampingAmplitude,
    tukeyRampingAmplitude
} from "./common.js"

let state = null

self.onmessage = (evt) => {
    try {
        if (!evt || !evt.data) throw new Error('Invalid event data')
        if (typeof evt.data !== 'object') throw new Error('Event data is not an object')
        state = new WorkerState(evt.data)
        getAverageAcousticPower()
    } catch (e) {
        console.error('Error processing message', e)
    }
}

function getAverageAcousticPower() {
    let averageAcousticPower = 0;
    const message = {
        job: 'average_acoustic_power',
        average_acoustic_power: averageAcousticPower,
    };

    if (state.pulseDur === 0) {
        self.postMessage(message)
        return
    }

    if (state.allTimeTogglesOn) {
        averageAcousticPower = calculateAVGAcousticPower(state.acoustic_power, state.PTRD, 'isptra');
    } else if (state.pulseAndTrainOnly) {
        averageAcousticPower = calculateAVGAcousticPower(state.acoustic_power, state.PTD, 'ispta');
    } else {
        averageAcousticPower = calculateAVGAcousticPower(state.acoustic_power, state.PD, 'isppa');
    }

    message.average_acoustic_power = +averageAcousticPower;
    self.postMessage(message);
}


function calculateAVGAcousticPower(acousticalPower, T, entity) {
    const timeInSeconds = T > 30 ? 30 : T
    const integral = trapezoidalRule(0, timeInSeconds, acousticalPower, entity, integrand);
    return +(integral / T).toFixed(2); // Divide by time span
}

function isppaPampl(t, acousticalPower) {
    let [
        R1s_p, // pulse ramping up start
        R1e_p, // pulse ramping up end
        R2s_p, // pulse ramping down start
        R2e_p, // pulse ramping down end
        off_time_p // pulse off time, pulse repetition interval - pulse duration
    ] = state.getISSPAParameters()

    let bump_up = off_time_p + R2e_p // pulse repetition interval
    // Shift pulse ramping window according to t
    while (t > R2e_p + off_time_p) {
        R1s_p += bump_up
        R1e_p += bump_up
        R2s_p += bump_up
        R2e_p += bump_up
    }

    // Apply pulse ramping if the pulse is detected
    if (R1s_p <= t && t <= R2e_p) {
        let pulse_ramp = 1 // no ramping
        if (state.pulse_ramp_duration_ms > 0) {
            pulse_ramp = state.pulse_ramp_shape === RAMPSHAPES.Linear
                ? linearRampingAmplitude(t, R1s_p, R1e_p, R2s_p, R2e_p)
                : tukeyRampingAmplitude(t, R1s_p, R1e_p, R2s_p, R2e_p)
        }
        acousticalPower *= pulse_ramp // adapt pressure amplitude accordingly
    } else {
        acousticalPower = 0 // no pulse, so pressure amplitude is 0
    }
    return {off_time_p, acousticalPower}
}

function isptaPampl(off_time_p, t, acousticalPower) {
    if (state.pulseTrainRampShape) {
        let [
            R1s_p_t, // pulse train ramping up start
            R1e_p_t, // pulse train ramping up end
            R2s_p_t, // pulse train ramping down start
            R2e_p_t, // pulse train ramping down end
            off_time_p_t // pulse train off time,
        ] = state.getISPTAParameters()

        let bump_up = R2e_p_t + off_time_p_t + off_time_p // pulse train repetition interval
        // Shift pulse train window according to t
        while (t > R2e_p_t + off_time_p_t + off_time_p) {
            R1s_p_t += bump_up
            R1e_p_t += bump_up
            R2s_p_t += bump_up
            R2e_p_t += bump_up
        }

        // Check if it's off time
        if (t > R2e_p_t && t <= (R2e_p_t + off_time_p_t + off_time_p)) {
            acousticalPower = 0
        } else {
            let pulse_t_ramp = 1 // no ramping
            if (state.pulseTrainRampDuration > 0) {
                pulse_t_ramp = state.pulseTrainRampShape === RAMPSHAPES.Linear
                    ? linearRampingAmplitude(t, R1s_p_t, R1e_p_t, R2s_p_t, R2e_p_t)
                    : tukeyRampingAmplitude(t, R1s_p_t, R1e_p_t, R2s_p_t, R2e_p_t)
            }
            acousticalPower *= pulse_t_ramp // adapt pressure amplitude accordingly
        }
    }
    return acousticalPower
}

function isptraPampl(t, value) {
    if (state.pulseTrainRepetitionRampShape) {
        const [
            R1s_p_t_r, // pulse train repetition ramping up start
            R1e_p_t_r, // pulse train repetition ramping up end
            R2s_p_t_r, // pulse train repetition ramping down start
            R2e_p_t_r, // pulse train repetition ramping down end
        ] = state.getISPTAPTRParameters()

        // Apply pulse train ramping
        let pulse_t_r_ramp = 1 // no ramping
        if (state.pulseTrainRepetitionRampDuration > 0) {
            pulse_t_r_ramp = state.pulseTrainRepetitionRampShape === RAMPSHAPES.Linear
                ? linearRampingAmplitude(t, R1s_p_t_r, R1e_p_t_r, R2s_p_t_r, R2e_p_t_r)
                : tukeyRampingAmplitude(t, R1s_p_t_r, R1e_p_t_r, R2s_p_t_r, R2e_p_t_r)
        }
        value *= pulse_t_r_ramp // adapt pressure amplitude accordingly
    }
    return value
}

function integrand(t, acousticalPower, entity) {
    // Define the basis integral function
    let basis_integral = Math.sin(2 * Math.PI * state.fund_freq * t) ** 2
    let [
        off_time_p // pulse off time, pulse repetition interval - pulse duration
    ] = state.getISSPAParameters()

    acousticalPower = isppaPampl(t, acousticalPower).acousticalPower;
    if (entity === 'isppa') {
        return acousticalPower * basis_integral
    }

    acousticalPower = isptaPampl(off_time_p, t, acousticalPower);
    if (entity === 'ispta') {
        return acousticalPower * basis_integral;
    }

    acousticalPower = isptraPampl(t, acousticalPower);
    return acousticalPower * basis_integral;
}
