import {
    trapezoidalRule,
    RAMPSHAPES,
    WorkerState,
    linearRampingAmplitude,
    tukeyRampingAmplitude
} from "./common.js"

let state = null
let Isppa_old = 19.26

self.onmessage = (evt) => {
    try {
        if (!evt || !evt.data) {
            throw new Error('Invalid event data')
        }

        if (state) {
            Isppa_old = state.isppaOld || 19.26
        }

        if (typeof evt.data !== 'object') {
            throw new Error('Event data is not an object')
        }
        state = new WorkerState(evt.data)
        if (!evt.data.time_parameters_valid) return

        const job = evt.data.job
        if (typeof job !== 'string') {
            throw new Error('Invalid job type')
        }

        switch (job) {
            case 'isppa':
            case 'isppaAfterPampl':
                getSignals(job)
                break
            case 'pampl':
                handlePamplCalculation(evt)
                break
            default:
                throw new Error('Unknown job type')
        }
    } catch (e) {
        console.error('Error processing message', e)
    }
}

// Handle pampl calculation jobs
function handlePamplCalculation(evt) {
    if (typeof +state.isppa !== 'number' || typeof +state.ppp_mpa !== 'number') {
        throw new Error('Invalid state properties for pampl calculation')
    }

    try {
        const newPampl = calculateNewPampl(Isppa_old, +state.isppa, +state.ppp_mpa)
        const derated_isppa = (+state.isppa * evt.data.d_reas_coeff ** 2 * evt.data.brain_pressure_transmission ** 2)
        const newPamplDerated = calculateNewPampl(+state.isppa, derated_isppa, newPampl)

        self.postMessage({
            job: 'pampl-result',
            ppp_mpa: +newPampl,
            derated_ppp_mpa: +newPamplDerated
        })

        state.isppaOld = +state.isppa
        state.derated_isppaOld = derated_isppa
    } catch (e) {
        console.error('Error calculating pampl', e)
    }
}

function getSignals(job) {
    if (state.pulseDur > 0) {
        const isppa = calculateISP(state.pampl, state.PD, 'isppa')
        const derated_isppa = calculateISP(state.derated_pampl, state.PD, 'isppa')
        let message = {
            job: job,
            isppa: +isppa.toFixed(2),
            derated_isppa: +derated_isppa.toFixed(2),

        }
        if (state.pulseAndTrainOnly || state.allTimeTogglesOn) {
            const ispta_pulse_train = calculateISP(state.pampl, state.PTD, 'ispta')
            const derated_ispta_pulse_train = calculateISP(state.derated_pampl, state.PTD, 'ispta')
            message.ispta_pulse_train = +ispta_pulse_train.toFixed(2)
            message.derated_ispta_pulse_train = +derated_ispta_pulse_train.toFixed(2)
        }

        if (state.allTimeTogglesOn) {
            const ispta_pulse_train_repetition = calculateISP(state.pampl, state.PTRD, 'isptra')
            const derated_ispta_pulse_train_repetition = calculateISP(state.derated_pampl, state.PTRD, 'isptra')
            message.ispta_pulse_train_repetition = +ispta_pulse_train_repetition.toFixed(2)
            message.derated_ispta_pulse_train_repetition = +derated_ispta_pulse_train_repetition.toFixed(2)
        }

        // set ISPPA as old
        state.isppaOld = isppa
        state.derated_isppaOld = derated_isppa
        self.postMessage(message)
    }
}

function calculateISP(pampl, T, entity) {
    let integral = trapezoidalRule(0, T, pampl, entity, integrand)
    return ((1 / (T * state.Z)) * integral) / 10 ** 4 // convert from W/m2 to W/cm2
}

// Function to calculate the new pampl value for a given scenario and desired Ispta or Isppa value
function calculateNewPampl(I_old, I_new, pampl) {
    if (!I_old) {
        I_old = I_new
    }
    // This formula adjusts the amplitude proportionally to the square root of the ratio of the desired I value to the old I value.
    return Math.sqrt(I_new / I_old) * pampl
}

function isppaPampl(t, pampl) {
    let [
        R1s_p, // pulse ramping up start
        R1e_p, // pulse ramping up end
        R2s_p, // pulse ramping down start
        R2e_p, // pulse ramping down end
        off_time_p // pulse off time, pulse repetition interval - pulse duration
    ] = state.getISSPAParameters()

    let bump_up = off_time_p + R2e_p // pulse repetition interval
    // Shift pulse ramping window according to t
    while (t > R2e_p + off_time_p) {
        R1s_p += bump_up
        R1e_p += bump_up
        R2s_p += bump_up
        R2e_p += bump_up
    }

    // Apply pulse ramping if the pulse is detected
    if (R1s_p <= t && t <= R2e_p) {
        let pulse_ramp = 1 // no ramping
        if (state.pulse_ramp_duration_ms > 0) {
            pulse_ramp = state.pulse_ramp_shape === RAMPSHAPES.Linear
                ? linearRampingAmplitude(t, R1s_p, R1e_p, R2s_p, R2e_p)
                : tukeyRampingAmplitude(t, R1s_p, R1e_p, R2s_p, R2e_p)
        }
        pampl *= pulse_ramp // adapt pressure amplitude accordingly
    } else {
        pampl = 0 // no pulse, so pressure amplitude is 0
    }
    return {off_time_p, pampl}
}

function isptaPampl(off_time_p, t, pampl) {
    if (state.pulseTrainRampShape) {
        let [
            R1s_p_t, // pulse train ramping up start
            R1e_p_t, // pulse train ramping up end
            R2s_p_t, // pulse train ramping down start
            R2e_p_t, // pulse train ramping down end
            off_time_p_t // pulse train off time,
        ] = state.getISPTAParameters()

        let bump_up = R2e_p_t + off_time_p_t + off_time_p // pulse train repetition interval
        // Shift pulse train window according to t
        while (t > R2e_p_t + off_time_p_t + off_time_p) {
            R1s_p_t += bump_up
            R1e_p_t += bump_up
            R2s_p_t += bump_up
            R2e_p_t += bump_up
        }

        // Check if it's off time
        if (t > R2e_p_t && t <= (R2e_p_t + off_time_p_t + off_time_p)) {
            pampl = 0
        } else {
            let pulse_t_ramp = 1 // no ramping
            if (state.pulseTrainRampDuration > 0) {
                pulse_t_ramp = state.pulseTrainRampShape === RAMPSHAPES.Linear
                    ? linearRampingAmplitude(t, R1s_p_t, R1e_p_t, R2s_p_t, R2e_p_t)
                    : tukeyRampingAmplitude(t, R1s_p_t, R1e_p_t, R2s_p_t, R2e_p_t)
            }
            pampl *= pulse_t_ramp // adapt pressure amplitude accordingly
        }
    }
    return pampl
}

function isptraPampl(t, pampl) {
    if (state.pulseTrainRepetitionRampShape) {
        const [
            R1s_p_t_r, // pulse train repetition ramping up start
            R1e_p_t_r, // pulse train repetition ramping up end
            R2s_p_t_r, // pulse train repetition ramping down start
            R2e_p_t_r, // pulse train repetition ramping down end
        ] = state.getISPTAPTRParameters()

        // Apply pulse train ramping
        let pulse_t_r_ramp = 1 // no ramping
        if (state.pulseTrainRepetitionRampDuration > 0) {
            pulse_t_r_ramp = state.pulseTrainRepetitionRampShape === RAMPSHAPES.Linear
                ? linearRampingAmplitude(t, R1s_p_t_r, R1e_p_t_r, R2s_p_t_r, R2e_p_t_r)
                : tukeyRampingAmplitude(t, R1s_p_t_r, R1e_p_t_r, R2s_p_t_r, R2e_p_t_r)
        }
        pampl *= pulse_t_r_ramp // adapt pressure amplitude accordingly
    }
    return pampl
}

function calculateIntegrandValue(pampl, basisIntegral) {
    const v = pampl * basisIntegral
    return Math.pow(v, 2)
}

function integrand(t, pampl, entity) {
    // Define the basis integral function
    let basis_integral = Math.sin(2 * Math.PI * state.fund_freq * t)
    let [
        off_time_p // pulse off time, pulse repetition interval - pulse duration
    ] = state.getISSPAParameters()

    pampl = isppaPampl(t, pampl).pampl
    if (entity === 'isppa') {
        return calculateIntegrandValue(pampl, basis_integral)
    }

    // Determine pulse train ramping if pulse is detected
    pampl = isptaPampl(off_time_p, t, pampl)
    if (entity === 'ispta') {
        return calculateIntegrandValue(pampl, basis_integral)
    }

    // Determine pulse train repetition ramping if pulse is detected
    pampl = isptraPampl(t, pampl)
    return calculateIntegrandValue(pampl, basis_integral)
}
