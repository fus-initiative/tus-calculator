import {expect, it} from "vitest"
import {
    calculateRamping,
    RAMPSHAPES
} from "./common.js"

it('calculates linear ramping', async () => {
    // Test utility function outside of calculator store, because of getters depending on multiple input
    const points = 100
    const rampsShape = RAMPSHAPES.Linear
    let orientation = "+"
    let ramp = calculateRamping(points, rampsShape, orientation)

    expect(ramp.length).toBe(100)
    expect(ramp[0]).toBe(0)
    expect(ramp[ramp.length-1].toFixed(2)).toBe("0.99")

    orientation = "-"
    ramp = calculateRamping(points, rampsShape, orientation)
    expect(ramp.length).toBe(100)
    expect(ramp[0]).toBe(1)
    expect(ramp[ramp.length-1].toFixed(2)).toBe("0.01")
})

it('calculates tukey ramping', async () => {
    // Test utility function outside of calculator store, because of getters depending on multiple input
    const points = 100
    const rampsShape = RAMPSHAPES.Tukey
    let orientation = "+"
    let ramp = calculateRamping(points, rampsShape, orientation)

    expect(ramp.length).toBe(100)
    expect(ramp[0]).toBe(0)
    expect(ramp[ramp.length-1].toFixed(2)).toBe("1.00")

    orientation = "-"
    ramp = calculateRamping(points, rampsShape, orientation)
    expect(ramp.length).toBe(100)
    expect(ramp[0]).toBe(1)
    expect(ramp[ramp.length-1].toFixed(2)).toBe("0.00")
})

