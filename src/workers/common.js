import {all, create} from "mathjs"

const config = {
    matrix: 'Array',
}

// import the function into math.js. Raw functions must be imported in the
// math namespace, they can't be used via `evaluate(scope)`.
export const math = create(all, config)
export const n = 10_000_000 // You can adjust this for more accuracy, trapezium rule steps
export const n_rep = 50_000_000 // You can adjust this for more accuracy, trapezium rule steps

export class WorkerState {
    acoustic_power = null
    nPulses = null
    pulseDur = null
    pulseRepetitionInterval = null
    pulseTrainDuration = null
    pulseTrainRampDuration = null
    pulseTrainRampShape = null
    pulseTrainRepetitionInterval = null
    pulseTrainRepetitionDuration = null
    pulseTrainRepetitionRampShape = null
    pulseTrainRepetitionRampDuration = null
    nTrains = null
    pulse_ramp_shape = null
    pulse_ramp_duration_ms = null
    pulseTrainRepetitionIntervalWidth = null
    pulseRepetitionIntervalWidth = null
    pulseOnly = null
    pulseAndTrainOnly = null
    allTimeTogglesOn = null
    fund_freq = null
    ppp_mpa = null
    derated_ppp_mpa = null
    z_Rayl = null
    linkColor = "rgb(238,227,227, 50)"
    canvas = null
    padding = 5
    arbitrary_default_amplitude = null
    amplitude = null
    sampleRate = null
    pulseSampleRate = null
    canvas_width = null
    canvas_height = null
    showPulseTrain = false
    showPulseTrainRepetition = false
    ctx = null
    axisColor = 'black'
    textColor = 'black'
    pulseFillStyle = 'red'
    backgroundColor = 'white'
    isDarkModeGlobal = false
    canvasBlock = null
    isppaOld = 19.26

    pulseTrainSampleRate = null
    pulseTrainRepetitionSampleRate = null

    constructor(data) {
        this.acoustic_power = data.acoustic_power
        this.pulseOnly = data.pulseOnly
        this.pulseAndTrainOnly = data.pulseAndTrainOnly
        this.allTimeTogglesOn = data.allTimeTogglesOn
        this.ppp_mpa = data.ppp_mpa ? data.ppp_mpa : null
        this.derated_ppp_mpa = data.derated_ppp_mpa
        this.fund_freq = data.frequency_MHz * 10 ** 6
        this.z_Rayl = data.z_Rayl
        this.pulseDur = data.pulseDur
        this.pulseRepetitionInterval = data.pulseRepetitionInterval
        this.pulseTrainDuration = data.pulseTrainDuration
        this.pulseTrainRampShape = data.pulseTrainRampShape
        this.pulseTrainRampDuration = data.pulseTrainRampDuration
        this.pulseTrainRepetitionInterval = data.pulseTrainRepetitionInterval
        this.pulseTrainRepetitionDuration = data.pulseTrainRepetitionDuration
        this.pulseTrainRepetitionRampShape = data.pulseTrainRepetitionRampShape
        this.pulseTrainRepetitionRampDuration = data.pulseTrainRepetitionRampDuration
        this.nPulses = data.nPulses
        this.nTrains = data.nTrains
        this.pulse_ramp_shape = data.pulse_ramp_shape
        this.pulse_ramp_duration_ms = data.pulse_ramp_duration_ms

        // used in calc newPampl
        this.isppa = data.isppa
        this.derated_isppa = data.derated_isppa
        this.isppaOld = data.isppaOld
        this.derated_isppaOld = data.derated_isppaOld

        this.arbitrary_default_amplitude = 100
        this.amplitude = 79

        this.sampleRate = data.sampleRate
        this.canvas_width = data.canvas_width || 1000
        this.canvas_height = data.canvas_height || 1000

        this.showPulseTrain = data.showPulseTrain
        this.showPulseTrainRepetition = data.showPulseTrainRepetition
        this.isDarkModeGlobal = data.isDarkModeGlobal

        if (this.isDarkModeGlobal) {
          this.axisColor = 'white'
          this.textColor = 'white'
          this.backgroundColor = '#1d1d1d'
          this.linkColor = "rgb(70,56,56)"
        } else {
          this.axisColor = 'black'
          this.textColor = 'black'
          this.backgroundColor = 'white'
          this.linkColor = "rgb(238,227,227, 50)"
        }

        this.pulseSampleRate = this.sampleRate
        this.pulseTrainSampleRate = this.sampleRate
        this.pulseTrainRepetitionSampleRate = this.sampleRate

        this.pulseTrainRepetitionIntervalWidth = this.canvas_width / this.nTrains
        this.pulseRepetitionIntervalWidth = this.canvas_width / this.nPulses
        this.canvasBlock = this.canvas_height / 10

        this.pampl = this.ppp_mpa * 10 ** 6
        this.derated_pampl = this.derated_ppp_mpa * 10 ** 6 // pressure amplitude in MPa

        this.PD = this.pulseDur / 1000 // pulse duration in seconds
        this.PTD = this.pulseTrainDuration / 1000 // pulse duration in seconds
        this.PTRD = this.pulseTrainRepetitionDuration / 1000 // pulse duration in seconds

        this.Z = this.z_Rayl * 10 ** 6 // acoustic impedance
    }

    getSignal(shapeWidth, pulsePressureSignalOff) {
        let pulseResult,
            pulsePressureSignalMaxAmpl,
            pulsePressureSignalRampUp,
            pulsePressureSignalRampDown

        if (this.pulse_ramp_duration_ms === 0) {
            pulseResult = math.concat(
                math.ones(Math.ceil(shapeWidth)),
                pulsePressureSignalOff
            )
        } else {
            const alphaOfPulse = (1 / this.pulseDur) * (this.pulse_ramp_duration_ms * 2)
            const rampWidth = shapeWidth * (alphaOfPulse / 2)
            pulsePressureSignalRampUp = calculateRamping(rampWidth, this.pulse_ramp_shape)
            pulsePressureSignalRampDown = calculateRamping(rampWidth, this.pulse_ramp_shape, '-')
            pulsePressureSignalMaxAmpl = math.ones(Math.floor(shapeWidth - (shapeWidth * alphaOfPulse)))
            pulseResult = math.concat(pulsePressureSignalRampUp,
                pulsePressureSignalMaxAmpl,
                pulsePressureSignalRampDown,
                pulsePressureSignalOff
            )
        }
        return pulseResult
    }

    getPulseSignal(shapeWidth) {
        let pulseResult,
            pulsePressureSignalMaxAmpl,
            pulsePressureSignalRampUp,
            pulsePressureSignalRampDown

        if (this.pulse_ramp_duration_ms === 0) {
            pulseResult = math.ones(Math.ceil(shapeWidth))
        } else {
            const alphaOfPulse = (1 / this.pulseDur) * (this.pulse_ramp_duration_ms * 2)
            const rampWidth = shapeWidth * (alphaOfPulse / 2)
            pulsePressureSignalRampUp = calculateRamping(rampWidth, this.pulse_ramp_shape)
            pulsePressureSignalRampDown = calculateRamping(rampWidth, this.pulse_ramp_shape, '-')
            pulsePressureSignalMaxAmpl = math.ones(Math.floor(shapeWidth - (shapeWidth * alphaOfPulse)))
            pulseResult = math.concat(pulsePressureSignalRampUp,
                pulsePressureSignalMaxAmpl,
                pulsePressureSignalRampDown
            )
        }
        return pulseResult
    }

    getISSPAParameters () {
        let R1s = 0 // start time of pulse ramping up in seconds
        let R1e = this.pulse_ramp_duration_ms / 1000 // end time of pulse ramping up in seconds
        let R2s = (this.pulseDur - this.pulse_ramp_duration_ms) / 1000 // start time of pulse ramping down in seconds
        let R2e = this.pulseDur / 1000 // end time of pulse ramping down in seconds
        let offTime = (this.pulseRepetitionInterval - this.pulseDur) / 1000
        return [ R1s, R1e, R2s, R2e, offTime ]
    }


    getISPTAParameters () {
        const R1s = 0 // start time of pulse ramping up in seconds
        const R1e = this.pulseTrainRampDuration / 1000 // end time of pulse ramping up in seconds
        const pulseOffTime = (this.pulseRepetitionInterval - this.pulseDur)

        const R2s = (this.pulseTrainDuration - pulseOffTime - this.pulseTrainRampDuration) / 1000 // start time of pulse ramping down in seconds
        const R2e = (this.pulseTrainDuration - pulseOffTime) / 1000 // end time of pulse ramping down in seconds
        const offTime = (this.pulseTrainRepetitionInterval - this.pulseTrainDuration) / 1000
        return [ R1s, R1e, R2s, R2e, offTime]
    }


    getISPTAPTRParameters () {
        let R1s = 0 // start time of pulse ramping up in seconds
        let R1e = this.pulseTrainRepetitionRampDuration / 1000 // end time of pulse ramping up in seconds

        const pulseOfftime = this.pulseRepetitionInterval - this.pulseDur
        const pulseTrainOfftime = this.pulseTrainRepetitionInterval - this.pulseTrainDuration

        let R2s = (this.pulseTrainRepetitionDuration - pulseOfftime - pulseTrainOfftime - this.pulseTrainRepetitionRampDuration) / 1000 // start time of pulse ramping down in seconds
        let R2e = (this.pulseTrainRepetitionDuration - pulseOfftime - pulseTrainOfftime) / 1000 // end time of pulse ramping down in seconds

        return [ R1s, R1e, R2s, R2e ]
    }

}


export function calculateRamping(points, rampShape, orientation) {
  if (rampShape === RAMPSHAPES.Linear) {
      return customLinSpace(
          0.0,
          1.0,
          1.0 / Math.floor(points),
          orientation)
  } else if (rampShape === RAMPSHAPES.Tukey) {
      return math.map(
          customLinSpace(
              0.0,
              0.5,
              0.5 / points,
              orientation),
          (x) => 0.5 * (1 + Math.cos((2 * Math.PI) * (x - 0.5)))
              .toFixed(6)
      )
  }
  return []
}

function customLinSpace(start, end, points, orientation) {
    return orientation === '-'
        ? math.range(end, start, -1 * points)
        : math.range(start, end, points)
}



export const GRAPHS = {
  Train: "train",
  Single: "pulse",
  Repetition: "repetition"
}

export const RAMPSHAPES = {
  Tukey: "Tukey",
  Linear: "Linear"
}

export function tukeyRampingAmplitude(t, R1s, R1e, R2s, R2e) {
    if (R1s <= t && t < R1e) {
        if (Math.abs(R1e - R1s) <= 1e-09) {
            return 1
        } else {
            return 0.5 * (1 + Math.cos(2 * Math.PI * (((t - R1s) / ((R1e - R1s) * 2)) - 0.5)))
        }
    } else if (R1e <= t && t <= R2s) {
        return 1
    } else if (R2s < t && t <= R2e) {
        if (Math.abs(R2e - R2s) <= 1e-09) {
            return 1
        } else {
            return 0.5 * (1 + Math.cos(2 * Math.PI * (((R2e - t) / ((R2e - R2s) * 2)) - 0.5)))
        }
    } else {
        return 0
    }
}

export function linearRampingAmplitude(t, R1s, R1e, R2s, R2e) {
    if (R1s <= t && t < R1e) {
        if (Math.abs(R1e - R1s) <= 1e-09) {
            return 1
        } else {
            return (t - R1s) / (R1e - R1s)
        }
    } else if (R1e <= t && t <= R2s) {
        return 1
    } else if (R2s < t && t <= R2e) {
        if (Math.abs(R2e - R2s) <= 1e-09) {
            return 1
        } else {
            return (R2e - t) / (R2e - R2s)
        }
    } else {
        return 0
    }
}

export function trapezoidalRule(a, b, value, entity, integrand) {
    const resolution = entity === 'isptra' ? n_rep : n
    const h = (b - a) / resolution
    let sum = 0.5 * (integrand(a, value, entity) + integrand(b, value, entity))
    for (let k = 1; k < resolution; k++) {
        sum += integrand(a + k * h, value, entity)
    }
    return h * sum
}
