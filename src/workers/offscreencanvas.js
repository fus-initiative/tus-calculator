import {
    GRAPHS,
    math,
    WorkerState,
    calculateRamping
} from "./common.js"

let ctx = null
let canvas = null
let state = null

onmessage = (evt) => {
  state = new WorkerState(evt.data)
  if (!evt.data.update) {
      canvas = evt.data.canvas
  }
  if (!canvas || !evt.data.time_parameters_valid) return
  canvas.width = state.canvas_width
  canvas.height = state.canvas_height

  postMessage({ loading: true })
  createTimingGraphsOnCanvas()
      .catch((err) => console.error(err))
      .finally(() =>
          postMessage({ loading: false })
      )
}

async function createTimingGraphsOnCanvas() {
    let centerX = 5
    const offset = 20
    const centerY = state.canvasBlock + offset
    ctx = canvas.getContext('2d', { alpha: true, antialias: 'subpixel' })
    ctx.fillStyle = state.backgroundColor
    ctx.fillRect(0, 0, state.canvas_width, state.canvas_height)
    ctx.lineWidth = 1
    ctx.strokeStyle = state.pulseFillStyle
    ctx.fillStyle = state.axisColor

    if (state.showPulseTrain) {
        if (state.showPulseTrainRepetition) drawPulseTrainRepetition(centerX,  state.canvasBlock * 7)
        drawPulseTrain(centerX, state.canvasBlock * 4.2)
    }
    drawPulse(centerX, centerY)
}

// Plotting pulse and pulse train
function drawPulse(centerX, centerY) {
    const title = GRAPHS.Single.charAt(0).toUpperCase() + GRAPHS.Single.slice(1)
    drawTitle(title, centerY - (state.amplitude / 2) - state.padding)
    drawLabels(centerX, centerY, GRAPHS.Single)
    pressureSignalToCanvas(calculatePulseSignal(state.canvas_width, GRAPHS.Single), centerX, centerY, GRAPHS.Single)
}

function drawPulseTrain(centerX, centerY) {
    drawPulseToTrainLink(centerX, state.canvasBlock * 3)
    drawLinkRectangle(GRAPHS.Train, centerX, centerY)
    drawTitle('Pulse train', centerY - state.amplitude / 2)
    drawLabels(centerX, centerY, GRAPHS.Train)
    pressureSignalToCanvas(calculatePulseTrainSignal(), centerX, centerY,  GRAPHS.Train)
}

function drawPulseTrainRepetition(centerX, centerY) {
    drawPulseTrainRepetitionToPulseTrainLink(centerX,  state.canvasBlock * 6)
    drawTitle('Pulse train repetition', state.canvasBlock * 7 - state.amplitude / 2)
    drawPulseTrainRepetitionLabels(centerX, centerY)
    pressureSignalToCanvas(calculatePulseTrainRepetitionSignal(), centerX, centerY, GRAPHS.Repetition)
}

function drawLabels(centerX, centerY, graph) {
    const canvasWidth = state.canvas_width
    const endpoint = 150
    const step = endpoint / 5
    const lineHeight = endpoint / 16
    let maxPulsesToDraw = graph === GRAPHS.Single ? 1 : state.nPulses
    const pulseDurX = canvasWidth
    const rampDurX = centerX + ((canvasWidth / state.pulseDur) * state.pulse_ramp_duration_ms)

    // const rampDurTrainRepetitionX = centerX + ((canvasWidth / state.pulseDur) * state.pulse_ramp_duration_ms)
    const pulseRepetitionIntervalX = (centerX + (canvasWidth / maxPulsesToDraw))
    const rampDurTrainX = centerX + (((canvasWidth / maxPulsesToDraw) / state.pulseRepetitionInterval) * state.pulseTrainRampDuration)

    const rampDurLabelHeight = endpoint - 2 * step
    const pulseDurLabelHeight = endpoint - step
    ctx.strokeStyle = state.axisColor
    ctx.font = "14px roboto"
    ctx.lineWidth = 2
    ctx.beginPath()
    ctx.setLineDash([2, 5])
    const arrowSize = 7

    if (graph === GRAPHS.Single) {
        ctx.moveTo(centerX, centerY)
        ctx.lineTo(centerX, centerY + pulseDurLabelHeight)
        ctx.moveTo(rampDurX, centerY)
        ctx.lineTo(rampDurX, centerY + rampDurLabelHeight)

        // Pulse duration
        ctx.moveTo(pulseDurX, centerY)
        ctx.lineTo(pulseDurX, centerY + pulseDurLabelHeight)
        ctx.stroke()

        ctx.setLineDash([])
        ctx.lineWidth = 2
        ctx.lineCap = "round"
        ctx.font = "14px roboto"
        ctx.textAlign = "center"

        if (state.pulse_ramp_duration_ms > 0) {
          ctx.fillText(`Ramp Duration: ${state.pulse_ramp_duration_ms}ms`, rampDurX > 0 ? rampDurX : centerX,
              centerY + rampDurLabelHeight - lineHeight)
          drawTwoWayArrow({ x: rampDurX, y: centerY + rampDurLabelHeight }, { x: centerX, y: centerY + rampDurLabelHeight }, arrowSize)
        }

        ctx.textAlign = pulseDurX > 300 ? "center" : "left"
        ctx.fillText(`Pulse Duration (PD): ${state.pulseDur}ms`, pulseDurX/2, centerY + pulseDurLabelHeight - lineHeight)

        ctx.closePath()
        drawTwoWayArrow( { x: pulseDurX, y: centerY + pulseDurLabelHeight },{ x: centerX, y: centerY + pulseDurLabelHeight }, arrowSize)
        // Pulse train

    } else {
        const startLastRamp = state.pulseTrainDuration - (state.pulseRepetitionInterval - state.pulseDur) - state.pulseTrainRampDuration
        const endLastRamp = state.pulseTrainDuration - (state.pulseRepetitionInterval - state.pulseDur)
        const lastRampStartX = centerX + (((canvasWidth / maxPulsesToDraw) / state.pulseRepetitionInterval) * startLastRamp)
        const lastRampEndX = centerX + (((canvasWidth / maxPulsesToDraw) / state.pulseRepetitionInterval) * endLastRamp)

        const rampDurLabelHeight = endpoint - 2 * step
        const priLabelHeight = endpoint - step
        const PulseTrainDurationHeight = endpoint

        // left
        ctx.moveTo(centerX, centerY)
        ctx.lineTo(centerX, centerY + PulseTrainDurationHeight)

        if (state.pulseTrainRampDuration > 0) {
            ctx.moveTo(rampDurTrainX, centerY)
            ctx.lineTo(rampDurTrainX, centerY + rampDurLabelHeight)

            ctx.moveTo(lastRampStartX, centerY)
            ctx.lineTo(lastRampStartX, centerY + rampDurLabelHeight)

            ctx.moveTo(lastRampEndX, centerY)
            ctx.lineTo(lastRampEndX, centerY + rampDurLabelHeight)
        }

        ctx.moveTo(pulseRepetitionIntervalX, centerY)
        ctx.lineTo(pulseRepetitionIntervalX , centerY + priLabelHeight)

        // right
        ctx.moveTo(canvasWidth, centerY)
        ctx.lineTo(canvasWidth, centerY + PulseTrainDurationHeight)
        ctx.stroke()

        ctx.setLineDash([])
        ctx.lineWidth = 2
        ctx.lineCap = "round"
        ctx.font = "14px roboto"
        ctx.textAlign = "left"

        if (state.pulseTrainRampDuration > 0) {
            const trainRampDurationLabel = `Pulse Train Ramp Duration: ${state.pulseTrainRampDuration}ms`
            ctx.fillText(trainRampDurationLabel, centerX, centerY + rampDurLabelHeight - lineHeight)

            drawTwoWayArrow( { x: rampDurTrainX, y: centerY + rampDurLabelHeight },
              { x: centerX, y: centerY + rampDurLabelHeight }, arrowSize)

            ctx.fillText(trainRampDurationLabel, lastRampStartX, centerY + rampDurLabelHeight - lineHeight)

            drawTwoWayArrow( { x: lastRampStartX, y: centerY + rampDurLabelHeight },
              { x: lastRampEndX, y: centerY + rampDurLabelHeight }, arrowSize)
        }


        ctx.textAlign = pulseRepetitionIntervalX > 300 ? "center" : "left"
        const priLabel = `Pulse Repetition Interval (PRI = 1 / PRF): ${state.pulseRepetitionInterval}ms`
        ctx.fillText(priLabel, pulseRepetitionIntervalX / 2, centerY + priLabelHeight - lineHeight)

        drawTwoWayArrow( { x: pulseRepetitionIntervalX - state.padding, y: centerY + priLabelHeight },
          { x: centerX, y: centerY + priLabelHeight }, arrowSize)

        ctx.textAlign = "center"
        const pdLabel = `Pulse Train Duration (PTD): ${state.pulseTrainDuration}ms`
        ctx.fillText(pdLabel, canvasWidth  / 2, centerY + PulseTrainDurationHeight - lineHeight)

        drawTwoWayArrow( { x: canvasWidth, y: centerY + PulseTrainDurationHeight },
            { x: centerX, y: centerY + PulseTrainDurationHeight }, arrowSize)

    }

    ctx.closePath()
    ctx.fontStyle = "italic"
    ctx.font = "italic 20px roboto"
    ctx.setLineDash([])
    ctx.textAlign = "center"

    ctx.fillText('t', pulseDurX + 30, centerY -20)
    drawArrow( { x: pulseDurX + 20, y: centerY - 10 },
      { x: pulseDurX + 50, y: centerY - 10 }, arrowSize)

    ctx.font = "14px roboto"
    ctx.lineWidth = 2
}

function drawPulseTrainRepetitionLabels(centerX, centerY) {
    const arrowSize = 7
    const canvasWidth = state.canvas_width
    const pulseRepetitionIntervalX = centerX + ((state.pulseTrainRepetitionIntervalWidth / state.pulseTrainRepetitionInterval) * state.pulseRepetitionInterval)
    const pulseTrainRepetitionIntervalX = (centerX + state.pulseTrainRepetitionIntervalWidth)
    const pulseTrainRampDurationX = centerX + ((state.pulseTrainRepetitionIntervalWidth / state.pulseTrainRepetitionInterval) * state.pulseTrainRepetitionRampDuration)
    const trainOffSet = state.pulseTrainRepetitionInterval - state.pulseTrainDuration
    const pulseOffset = state.pulseRepetitionInterval - state.pulseDur
    const offSet = trainOffSet + pulseOffset
    const startLastRamp = state.pulseTrainRepetitionDuration - offSet - state.pulseTrainRepetitionRampDuration
    const endLastRamp = state.pulseTrainRepetitionDuration - offSet
    const lastRampStartX = centerX + ((canvasWidth / state.pulseTrainRepetitionDuration) * startLastRamp)
    const lastRampEndX = centerX + ((canvasWidth / state.pulseTrainRepetitionDuration) * endLastRamp)
    const endpoint = 150
    const step = endpoint / 5
    const rampLabelHeight = endpoint - 3 * step
    const pulseRepetitionIntervalHeight = endpoint - 2 * step
    const pulseDurLabelHeight = endpoint - step
    const pulseTrainRepetitionIntervalHeight = endpoint

    ctx.strokeStyle = state.axisColor
    ctx.lineWidth = 2
    ctx.beginPath()
    ctx.setLineDash([2, 5])

    ctx.moveTo(centerX, centerY)
    ctx.lineTo(centerX, centerY + pulseTrainRepetitionIntervalHeight)


    if (state.pulseTrainRepetitionRampDuration > 0) {
        ctx.moveTo(pulseTrainRampDurationX, centerY)
        ctx.lineTo(pulseTrainRampDurationX, centerY + rampLabelHeight)

        ctx.moveTo(lastRampStartX, centerY)
        ctx.lineTo(lastRampStartX, centerY + rampLabelHeight)

        ctx.moveTo(lastRampEndX, centerY)
        ctx.lineTo(lastRampEndX, centerY + rampLabelHeight)
    }


    ctx.moveTo(pulseRepetitionIntervalX, centerY)
    ctx.lineTo(pulseRepetitionIntervalX, centerY + pulseRepetitionIntervalHeight)

    ctx.moveTo(pulseTrainRepetitionIntervalX, centerY)
    ctx.lineTo(pulseTrainRepetitionIntervalX, centerY + pulseDurLabelHeight)

    // Pulse repetition Interval
    ctx.moveTo(state.canvas_width, centerY)
    ctx.lineTo(state.canvas_width, centerY + pulseTrainRepetitionIntervalHeight)
    ctx.stroke()

    ctx.setLineDash([])
    ctx.lineWidth = 2
    ctx.lineCap = "round"
    ctx.font = "14px roboto"

    if (state.pulseTrainRepetitionRampDuration > 0) {
        const trainRampDurationLabel = `PTR Ramp Duration: ${state.pulseTrainRepetitionRampDuration}ms`
        ctx.textAlign =  "left"
        ctx.fillText(
            trainRampDurationLabel,
            centerX + state.padding,
            centerY + rampLabelHeight - state.padding
        )
        drawTwoWayArrow(
            {x: centerX, y: centerY + rampLabelHeight},
            {x: pulseTrainRampDurationX, y: centerY + rampLabelHeight},
            arrowSize
        )
        drawTwoWayArrow(
            { x: lastRampStartX, y: centerY + rampLabelHeight },
            { x: lastRampEndX, y: centerY + rampLabelHeight }, arrowSize
        )
    }

    ctx.textAlign = pulseRepetitionIntervalX > 300 ? "center" : "left"
    ctx.fillText(
      `Pulse Repetition Interval: ${state.pulseRepetitionInterval}ms`,
      pulseRepetitionIntervalX / 2,
      centerY + pulseRepetitionIntervalHeight - state.padding
    )

    drawTwoWayArrow(
      { x: centerX, y: centerY + pulseRepetitionIntervalHeight },
      { x: pulseRepetitionIntervalX - state.padding, y: centerY + pulseRepetitionIntervalHeight },
          arrowSize)

      ctx.textAlign = pulseTrainRepetitionIntervalX > 300 ? "center" : "left"
      ctx.fillText(
          `Pulse Train Repetition Interval: ${state.pulseTrainRepetitionInterval}ms`,
          pulseTrainRepetitionIntervalX / 2,
          centerY + pulseDurLabelHeight - state.padding
      )

    drawTwoWayArrow(
      { x: centerX, y: centerY + pulseDurLabelHeight },
      { x: pulseTrainRepetitionIntervalX - state.padding , y: centerY + pulseDurLabelHeight },
          arrowSize)

      ctx.textAlign = state.canvas_width > 300 ? "center" : "left"
      ctx.fillText(
          `Pulse Train Repetition Duration: ${state.pulseTrainRepetitionDuration}ms`,
          state.canvas_width / 2,
          centerY + pulseTrainRepetitionIntervalHeight - state.padding
      )

      drawTwoWayArrow(
          { x: centerX, y: centerY + pulseTrainRepetitionIntervalHeight },
          { x: state.canvas_width, y: centerY + pulseTrainRepetitionIntervalHeight },
          arrowSize)

      ctx.textAlign = "left"
      ctx.font = "14px roboto"
      ctx.lineWidth = 3
}

function calculatePulseSignal(pulseDurWidth, shape) {
    if (GRAPHS.Train === shape) {
        const pulsePressureSignalOff = math.zeros(Math.floor(state.pulseRepetitionIntervalWidth - pulseDurWidth))
        return state.getSignal(pulseDurWidth, pulsePressureSignalOff)
    }
    return state.getPulseSignal(pulseDurWidth)
}

function calculateValuesPulseTrainRampShapeForRepetition() {
    let pulsePressureSignalRampUp = [], pulsePressureSignalMaxAmpl = [], pulsePressureSignalRampDown = []

    const timePerPixel = state.pulseTrainRepetitionInterval / state.pulseTrainRepetitionIntervalWidth
    const tailDurationWidth = (state.pulseRepetitionInterval - state.pulseDur) / timePerPixel
    const shapeWidth = state.pulseDur / timePerPixel

    let pulsePressureSignalOff = math.zeros(Math.floor(tailDurationWidth))
    if (state.pulse_ramp_duration_ms === 0) {
        pulsePressureSignalMaxAmpl = math.ones(Math.ceil(shapeWidth))
    } else {
        const alphaOfPulse = math.floor((1 / state.pulseDur) * (state.pulse_ramp_duration_ms * 2))
        const rampWidth = math.floor(shapeWidth * (alphaOfPulse / 2))
        pulsePressureSignalRampUp = calculateRamping(rampWidth, state.pulse_ramp_shape)
        pulsePressureSignalMaxAmpl = math.ones(Math.floor(shapeWidth - (2 * rampWidth)))
        pulsePressureSignalRampDown = calculateRamping(rampWidth, state.pulse_ramp_shape, '-')
    }

    const pulseSignal = math.concat(
        pulsePressureSignalRampUp,
        pulsePressureSignalMaxAmpl,
        pulsePressureSignalRampDown,
        pulsePressureSignalOff
    )

    return {
        pulseSignal: pulseSignal,
        tail: pulsePressureSignalOff
    }
}

function calculatePulseTrainSignal() {
    let pulseTrainSignals = []
    const pulseShapeWidth = state.pulseDur / (state.pulseRepetitionInterval / state.pulseRepetitionIntervalWidth)

    // for the pulse train specific parameters calculate the values for a single pulse and its tail
    let results = calculatePulseSignal(pulseShapeWidth, GRAPHS.Train)
    const tailLength = Math.floor(state.pulseRepetitionIntervalWidth - pulseShapeWidth)
    const tail = math.zeros(tailLength)


    // create the train
    for (let pulse = 0; pulse < Math.ceil(state.nPulses); pulse += 1) {
        pulseTrainSignals = pulseTrainSignals.concat(results)
    }

    if (state.pulseTrainRampDuration === 0) {
        return pulseTrainSignals
    }

    const alphaOfPulseTrain = (1 / (state.pulseTrainDuration - (state.pulseRepetitionInterval - state.pulseDur)) * (state.pulseTrainRampDuration))
    const pulseTrainShapeWidth = pulseTrainSignals.length - tailLength
    let rampWidth = Math.floor(pulseTrainShapeWidth * alphaOfPulseTrain)

    let tPulseTrainRampUp = calculateRamping(rampWidth, state.pulseTrainRampShape)
    const rampDown = calculateRamping(rampWidth, state.pulseTrainRampShape, '-')
    const startSignalMax = tPulseTrainRampUp.length
    const endSignalMax = pulseTrainSignals.length - tPulseTrainRampUp.length - tailLength
    const signalMax = pulseTrainSignals.slice(startSignalMax, endSignalMax)
    const signalRampUp = math.map(pulseTrainSignals.slice(0, startSignalMax), (v, i) => v * tPulseTrainRampUp[i])
    const signalRampDown = math.map(pulseTrainSignals.slice(endSignalMax), (v, i) => v * rampDown[i])

    pulseTrainSignals = []
    return math.concat(signalRampUp, signalMax, signalRampDown, tail)
}

function calculateTrainForTrainRepetition(pulseTrainPressureSignals,
                                          results,
                                          alphaOfPulseTrain,
                                          tPulseTrainRampUp,
                                          tPulseTrainRampDown,
                                          PulseTrainRepetitionIntervalTailLength) {
    let pressureSignal = []
    // Apply pulse train ramping to single train
    // Alpha needs to be calculated over the pulse train duration minus the last tail
    let pulseTrainShapeWidth = pulseTrainPressureSignals.length - results.tail.length - PulseTrainRepetitionIntervalTailLength
    let pulseTrainRampWidth = Math.floor(pulseTrainShapeWidth * alphaOfPulseTrain)
    tPulseTrainRampUp = calculateRamping(pulseTrainRampWidth, state.pulseTrainRampShape)
    tPulseTrainRampDown = calculateRamping(pulseTrainRampWidth, state.pulseTrainRampShape, '-')

    if (pulseTrainRampWidth > 0) {
        const boundaryRampUpToMax = tPulseTrainRampUp.length
        const signalRampUp = pulseTrainPressureSignals
            .slice(0, boundaryRampUpToMax)
            .map((value, index) => value * tPulseTrainRampUp[index])

        const boundaryMaxToRampDown = pulseTrainPressureSignals.length - tPulseTrainRampUp.length - results.tail.length - PulseTrainRepetitionIntervalTailLength
        const signalMax = pulseTrainPressureSignals.slice(boundaryRampUpToMax, boundaryMaxToRampDown)
        const signalRampDown = pulseTrainPressureSignals.slice(boundaryMaxToRampDown, -results.tail.length - PulseTrainRepetitionIntervalTailLength)
            .map((value, index) => value * tPulseTrainRampDown[index])
        pressureSignal = signalRampUp.concat(
            signalMax,
            signalRampDown,
            results.tail,
            Array(PulseTrainRepetitionIntervalTailLength).fill(0)
        )
    }
    return pressureSignal
}

function calculatePulseTrainRepetitionSignal() {
    let pulseTrainRepTotalT = [],
        tPulseTrainRampUp = [],
        tPulseTrainRampDown = [],
        pulseTrainPulseSignals = []

    // Calculate the pulse (with ramping)
    // results for a single pulse train that can be ramped
    const results = calculateValuesPulseTrainRampShapeForRepetition()
    const alphaOfPulseTrain = (1 / (state.pulseTrainDuration - (state.pulseRepetitionInterval - state.pulseDur)) * (state.pulseTrainRampDuration))

    // Use pulses to create the pulse train
    for (let pulse = 0; pulse < state.nPulses; pulse += 1) {
        pulseTrainPulseSignals = pulseTrainPulseSignals.concat(results.pulseSignal)
    }

    // Add the extra tail added to the train caused by the Pulse Train Repetition Interval
    const timePerPixel = state.pulseTrainRepetitionInterval / state.pulseTrainRepetitionIntervalWidth
    const PulseTrainRepetitionIntervalTailWidth = (state.pulseTrainRepetitionInterval - state.pulseTrainDuration) / timePerPixel
    const PulseTrainRepetitionIntervalTail = PulseTrainRepetitionIntervalTailWidth > 0
        ? math.zeros(Math.floor(PulseTrainRepetitionIntervalTailWidth)) : []

    pulseTrainPulseSignals = pulseTrainPulseSignals.concat(PulseTrainRepetitionIntervalTail)

    let trainSignalforRepetitionGraph = pulseTrainPulseSignals
    if (state.pulseTrainRampDuration > 0) {
        trainSignalforRepetitionGraph = calculateTrainForTrainRepetition(
            pulseTrainPulseSignals,
            results,
            alphaOfPulseTrain,
            tPulseTrainRampUp,
            tPulseTrainRampDown,
            PulseTrainRepetitionIntervalTail.length
        )
    }

    for (let train = 0; train < state.nTrains; train += 1) {
      pulseTrainRepTotalT = pulseTrainRepTotalT.concat(trainSignalforRepetitionGraph)
    }

    // CREATE PULSE TRAIN REPETITION AND APPLY RAMPING
    const alphaOfPulseTrainRepetition = (1 / state.pulseTrainRepetitionDuration) * (state.pulseTrainRepetitionRampDuration)
    let trainRepRampWidth = Math.floor(((state.canvas_width)) * (alphaOfPulseTrainRepetition))

    if (state.pulseTrainRepetitionRampDuration === 0 || trainRepRampWidth === 0) return pulseTrainRepTotalT

    const t_pulse_train_rep_ramp_up = calculateRamping(trainRepRampWidth, state.pulseTrainRepetitionRampShape)
    const reverseTrainRepRamp = calculateRamping(trainRepRampWidth, state.pulseTrainRepetitionRampShape, '-')

    const pulseTrainRepRampUp = pulseTrainRepTotalT
        .slice(0, t_pulse_train_rep_ramp_up.length)
        .map((value, index) => value * t_pulse_train_rep_ramp_up[index])


    const startSignalMax = t_pulse_train_rep_ramp_up.length
    const endSignalMax = pulseTrainRepTotalT.length - t_pulse_train_rep_ramp_up.length - results.tail.length - PulseTrainRepetitionIntervalTail.length
    const between = pulseTrainRepTotalT.slice(startSignalMax, endSignalMax)
    const pulseTrainRepRampDown = pulseTrainRepTotalT.slice(endSignalMax, endSignalMax + reverseTrainRepRamp.length)
        .map((value, index) => value * reverseTrainRepRamp[index])

    const signalLength = pulseTrainRepRampUp.length
        + (endSignalMax - startSignalMax)
        + pulseTrainRepRampDown.length

    const signalOff = math.zeros(Math.floor(pulseTrainRepTotalT.length - signalLength))
    return math.concat(pulseTrainRepRampUp, between, pulseTrainRepRampDown, signalOff)
}

function getSampleRate(graph) {
    if (graph === GRAPHS.Train) return state.pulseTrainSampleRate
    if (graph === GRAPHS.Repetition) return state.pulseTrainRepetitionSampleRate
    return state.pulseSampleRate
}


function pressureSignalToCanvas(arr, startX, startY, graph) {
    let resolution = 1
    const multiplier = 25
    const arrLength = arr.length
    let sRate = getSampleRate(graph)
    if (graph === GRAPHS.Train) {
        resolution = state.pulseTrainDuration / state.pulseDur
        sRate =  getSampleRate(graph) / resolution

    } else if (graph === GRAPHS.Repetition) {
        const trainResolution = state.pulseTrainDuration / state.pulseDur
        const repetitionResolution = state.pulseTrainRepetitionDuration / state.pulseRepetitionInterval

        resolution = (trainResolution > repetitionResolution) ? trainResolution : repetitionResolution
        sRate =  getSampleRate(graph) / resolution
    }

    ctx.setLineDash([])
    ctx.fillStyle = state.pulseFillStyle
    ctx.strokeStyle = state.pulseFillStyle // Set the pulse color
    ctx.lineWidth = 0.8
    ctx.strokeWidth = 0.2

    ctx.save()
    ctx.translate(startX, startY)
    ctx.beginPath()

    let pulseArrayIndex = 0
    for (let pixelIndex=0; pixelIndex < arrLength; pixelIndex++) {

        if (arr[pixelIndex - 1] === 0 && arr[pixelIndex] > 0) pulseArrayIndex = 0

        let value = isNaN(arr[pixelIndex]) ? 0 : arr[pixelIndex]
        let y = value * (state.amplitude) /3

        if (([GRAPHS.Train, GRAPHS.Repetition].includes(graph)  && resolution >= 6)) {
            ctx.lineTo(pixelIndex, y)
            ctx.lineTo(pixelIndex, -1 * y)
        } else {
            const drawn_y = (y * Math.sin(((Math.PI * pulseArrayIndex) / sRate.toFixed(2)) * multiplier)) * -1
            ctx.lineTo(pixelIndex, drawn_y)
        }
        ctx.stroke()
        pulseArrayIndex++
    }
    ctx.restore()
    ctx.fillStyle = state.textColor
}

function drawPulseToTrainLink(centerX, centerY) {
  const top = state.canvasBlock + 20 + state.amplitude / 2
  if (state.nPulses > 1) {
    ctx.lineWidth = 4
    ctx.beginPath()

      const gradient = ctx.createLinearGradient(
        centerX,
        centerY + state.canvasBlock - state.amplitude - state.padding,
        centerX,
        top
    )

    // Add three color stops
    gradient.addColorStop(0, state.linkColor)
    gradient.addColorStop(0.75, state.linkColor)
    gradient.addColorStop(1, state.backgroundColor)

    ctx.fillStyle = gradient

    const pulseWidth = (state.pulseRepetitionIntervalWidth / state.pulseRepetitionInterval) * state.pulseDur

      // bottom right
    ctx.moveTo(
        centerX + pulseWidth,
        centerY + state.canvasBlock - state.amplitude / 2
    )

    // top right
    ctx.lineTo(
        centerX + state.pulseRepetitionIntervalWidth * state.nPulses - state.padding,
        top
    )

    // top left
    ctx.lineTo(
        centerX,
        top
    )

    // bottom left
    ctx.lineTo(
        centerX,
        centerY + state.canvasBlock - state.amplitude / 2
    )

    ctx.closePath()
    ctx.fill()
    ctx.lineWidth = 3
    ctx.strokeStyle = state.pulseFillStyle
    ctx.fillStyle = state.axisColor
  }
}

function drawPulseTrainRepetitionToPulseTrainLink(centerX, centerY) {
    const pulseTrainDurationWidth = state.pulseTrainDuration / (state.pulseTrainRepetitionInterval / state.pulseTrainRepetitionIntervalWidth)
    const pWidth = pulseTrainDurationWidth / state.nPulses
    ctx.fillStyle = state.linkColor
    ctx.lineWidth = 4
    if (state.nTrains > 1) {
        ctx.beginPath()
        ctx.setLineDash([2, 5])
        const gradient = ctx.createLinearGradient(
            centerX, centerY + state.amplitude / 2,
            centerX, centerY - state.amplitude
        )
        gradient.addColorStop(0, state.linkColor)
        gradient.addColorStop(0.75, state.linkColor)
        gradient.addColorStop(1, state.backgroundColor)
        ctx.fillStyle = gradient

        // bottom right
        ctx.moveTo(
            centerX + pWidth * state.nPulses - state.padding,
            centerY + state.canvasBlock - 50
        )
        // top right
        ctx.lineTo(state.canvas_width, centerY - state.amplitude)
        // top left
        ctx.lineTo(centerX , centerY - state.amplitude)
        // bottom left
        ctx.lineTo(centerX, centerY + state.canvasBlock - 50)

        ctx.closePath()
        ctx.fill()
        ctx.setLineDash([])
        const gradientRect = ctx.createLinearGradient(
            centerX, centerY + state.amplitude + 6*state.padding,
            centerX, centerY
        )
        gradientRect.addColorStop(1, state.linkColor)
        gradientRect.addColorStop(0.75, state.linkColor)
        gradientRect.addColorStop(0, state.backgroundColor)
        ctx.fillStyle = gradientRect

        ctx.fillRect(
            centerX,
            centerY ,
            pWidth * state.nPulses,
            state.amplitude + 6*state.padding)
    }

    ctx.lineWidth = 3
    ctx.strokeStyle = state.pulseFillStyle
    ctx.fillStyle = state.axisColor
}

function drawTwoWayArrow(p1, p2, size) {
    drawArrow(p1, p2, size)
    drawArrow(p2, p1, size)
}

function drawArrow(p1, p2, size) {
    const angle = Math.atan2((p2.y - p1.y), (p2.x - p1.x))
    const hyp = Math.sqrt((p2.x - p1.x) * (p2.x - p1.x) + (p2.y - p1.y) * (p2.y - p1.y))
    ctx.save()
    ctx.translate(p1.x, p1.y)
    ctx.rotate(angle)

    // line
    ctx.beginPath()
    ctx.moveTo(0, 0)
    ctx.lineTo(hyp - size, 0)
    ctx.stroke()
    ctx.closePath()
    // triangle
    ctx.beginPath()
    ctx.lineTo(hyp - size, size)
    ctx.lineTo(hyp, 0)
    ctx.lineTo(hyp - size, -size)
    ctx.closePath()
    ctx.fill()
    ctx.restore()
}

function drawTitle(title, y) {
    ctx.font = "20px roboto"
    ctx.textAlign = "center"
    ctx.fillText(title, state.canvas_width / 2, y)
    ctx.textAlign = "left"
}

function drawLinkRectangle(graph, centerX, centerY) {
    if (graph !== GRAPHS.Single && state.nPulses > 1) {
        const gradientRect = ctx.createLinearGradient(
            centerX,
            centerY + state.amplitude,
            centerX,
            centerY - state.amplitude
        )
        gradientRect.addColorStop(0, state.backgroundColor)
        gradientRect.addColorStop(0.95, state.linkColor)
        gradientRect.addColorStop(1, state.linkColor)

        ctx.fillStyle = gradientRect

        const pulseWidth = (state.pulseRepetitionIntervalWidth / state.pulseRepetitionInterval) * state.pulseDur
        ctx.fillRect(
            centerX,
            centerY - state.amplitude,
            pulseWidth,
            1.5 * state.amplitude
        )
        ctx.closePath()
        ctx.lineWidth = 3
        ctx.strokeStyle = state.pulseFillStyle
        ctx.fillStyle = state.axisColor
    }
}