import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { quasar, transformAssetUrls } from '@quasar/vite-plugin'

// https://vitejs.dev/config/
export default defineConfig({
    plugins: [
        vue({
            template: { transformAssetUrls }
        }),
        quasar({
            sassVariables: 'src/quasar-variables.sass'
        })
    ],
    test:{
        testTimeout: 100000,
        setupFiles: ['./vitest.setup.ts'],
        server: {
            deps: {
                inline: ['vitest-canvas-mock'],
            }
        },
        globals: true,
        environment: 'jsdom',
        exclude: ['packages/template/*', 'node_modules/**/*', 'e2e/**'],
        threads: false,
        environmentOptions: {
          jsdom: {
            resources: 'usable',
          },
        },
    }
})